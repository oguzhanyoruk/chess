#include "BitIterator.h"

#include "MoveLookupTable.h"

namespace {
static inline void next(u64 const& container, u64& current) {
    while (current) {
        current >>= 1;
        if (current & container) {
            break;
        }
    }
}

static inline void prev(u64 const& container, u64& current) {
    u64 leftMask = ~((current << 1) - 1) & container;
    current = ~(leftMask - 1) & leftMask;
}

inline u64& moveNorth(u64& at) {
    return at <<= 8;
};

inline u64& moveNorthEast(u64& at) {
    return at <<= 7;
};

inline u64& move2North1East(u64& at) {
    return at <<= 15;
};

inline u64& move1North2East(u64& at) {
    return at <<= 6;
};

inline u64& moveEast(u64& at) {
    return at >>= 1;
};

inline u64& move2South1East(u64& at) {
    return at >>= 17;
};

inline u64& move1South2East(u64& at) {
    return at >>= 10;
};

inline u64& moveSouthEast(u64& at) {
    return at >>= 9;
};

inline u64& moveSouth(u64& at) {
    return at >>= 8;
};

inline u64& moveSouthWest(u64& at) {
    return at >>= 7;
};

inline u64& move2South1West(u64& at) {
    return at >>= 15;
};

inline u64& move1South2West(u64& at) {
    return at >>= 6;
};

inline u64& moveWest(u64& at) {
    return at <<= 1;
};

inline u64& moveNorthWest(u64& at) {
    return at <<= 9;
};

inline u64& move2North1West(u64& at) {
    return at <<= 17;
};

inline u64& move1North2West(u64& at) {
    return at <<= 10;
};


static constexpr u64 MoveIteratorEnd = 0xFFFFFFFFFFFFFFFF;
}

BitIterator::BitIterator(const u64 &container)
    : current(0x0000000000000001),
      container(container)
{
    if (!(current & container)) {
        prev(container, current);
    }
}

BitIterator &BitIterator::operator++() {
    prev(container, current);
    return *this;
}

u64 BitIterator::operator*() {
    return current;
}

BitIterator::operator bool()
{
    return current;
}

bool operator==(BitIterator const& lhs, BitIterator const& rhs) {
    return lhs.current & rhs.current;
}

bool operator!=(BitIterator const& lhs, BitIterator const& rhs) {
    return !(lhs == rhs);
}

MoveIterator::MoveIterator(u64 const& start)
    : start(start),
      direction(Direction::N),
      current(MoveIteratorEnd),
      moves(0x0)
{}

u64 MoveIterator::operator*()
{
    return current;
}

MoveIterator::operator bool()
{
    return !static_cast<bool>(MoveIteratorEnd ^ current);
}


const MoveIterator& MoveIterator::end()
{
    static const auto e = MoveIterator();
    return e;
}

const u64 &MoveIterator::getMoves() const
{
    return moves;
}

MoveIterator::MoveIterator()
    : start(0x0),
      direction(Direction::END),
      current(MoveIteratorEnd)
{}

DirectionIterator::DirectionIterator(const u64 &start, const u64 &directionGuide)
    : MoveIterator(start)
{
    current = start;
    if ((moves = QueenMoveTable::NMoves(start)) & directionGuide) {
        direction = Direction::N;
    } else if ((moves = QueenMoveTable::NEMoves(start)) & directionGuide) {
        direction = Direction::NE;
    } else if ((moves = QueenMoveTable::EMoves(start)) & directionGuide) {
        direction = Direction::E;
    } else if ((moves = QueenMoveTable::SEMoves(start)) & directionGuide) {
        direction = Direction::SE;
    } else if ((moves = QueenMoveTable::SMoves(start)) & directionGuide) {
        direction = Direction::S;
    } else if ((moves = QueenMoveTable::SWMoves(start)) & directionGuide) {
        direction = Direction::SW;
    } else if ((moves = QueenMoveTable::WMoves(start)) & directionGuide) {
        direction = Direction::W;
    } else if ((moves = QueenMoveTable::NWMoves(start)) & directionGuide) {
        direction = Direction::NW;
    } else {
        moves = 0x0;
        direction = Direction::END;
        current = MoveIteratorEnd;
        return;
    }
    ++*this;
}

DirectionIterator::DirectionIterator(const u64 &start, MoveIterator::Direction direction)
    : MoveIterator(start)
{
    current = start;
    this->direction = direction;
    switch (direction) {
        case MoveIterator::N:
            moves = QueenMoveTable::NMoves(start);
            break;
        case MoveIterator::NE:
            moves = QueenMoveTable::NEMoves(start);
            break;
        case MoveIterator::E:
            moves = QueenMoveTable::EMoves(start);
            break;
        case MoveIterator::SE:
            moves = QueenMoveTable::SEMoves(start);
            break;
        case MoveIterator::S:
            moves = QueenMoveTable::SMoves(start);
            break;
        case MoveIterator::SW:
            moves = QueenMoveTable::SWMoves(start);
            break;
        case MoveIterator::W:
            moves = QueenMoveTable::WMoves(start);
            break;
        case MoveIterator::NW:
            moves = QueenMoveTable::NWMoves(start);
            break;
        default:
            current = MoveIteratorEnd;
            return;
    }
    ++*this;
}

DirectionIterator &DirectionIterator::operator++()
{
    switch (direction) {
        case MoveIterator::N:
            if (!(moves & moveNorth(current))) {
                current = MoveIteratorEnd;
            }
            break;
        case MoveIterator::NE:
            if (!(moves & moveNorthEast(current))) {
                current = MoveIteratorEnd;
            }
        break;
        case MoveIterator::E:
            if (!(moves & moveEast(current))) {
                current = MoveIteratorEnd;
            }
            break;
        case MoveIterator::SE:
            if (!(moves & moveSouthEast(current))) {
                current = MoveIteratorEnd;
            }
            break;
        case MoveIterator::S:
            if (!(moves & moveSouth(current))) {
                current = MoveIteratorEnd;
            }
            break;
        case MoveIterator::SW:
            if (!(moves & moveSouthWest(current))) {
                current = MoveIteratorEnd;
            }
            break;
        case MoveIterator::W:
            if (!(moves & moveWest(current))) {
                current = MoveIteratorEnd;
            }
            break;
        case MoveIterator::NW:
            if (!(moves & moveNorthWest(current))) {
                current = MoveIteratorEnd;
            }
            break;
        default:
            break;
    }
    return *this;
}

DirectionIterator DirectionIterator::end()
{
    static DirectionIterator e(0x0, 0x0);
    e.current = 0xFFFFFFFFFFFFFFFF;
    return e;
}

bool DirectionIterator::isDiagonal()
{
    return direction & Direction::DIAGONAL;
}

u64 DirectionIterator::getMoves()
{
    if (!direction || !current || current == 0xFFFFFFFFFFFFFFFF) {
        return 0xFFFFFFFFFFFFFFFF;
    }
    return current | ((Direction::UPPER & direction) ? (moves & ~(current - 1)) ^ current : moves & (current - 1));
}

PawnMoveIterator::PawnMoveIterator(const u64 &start, PieceColor color)
    : MoveIterator(start),
      color(color)
{
    moves = color ? moveNorth(current = start) : moveSouth(current = start);
    if (!current) {
        current = MoveIteratorEnd;
    }
}

PawnMoveIterator &PawnMoveIterator::operator++()
{
    if (color ? (current & mask::rank::_3) : (current & mask::rank::_6)) {
        color ? moveNorth(current) : moveSouth(current);
    }
    else {
        current = MoveIteratorEnd;
    }
    return *this;
}

PawnCaptureIterator::PawnCaptureIterator(const u64 &start, PieceColor color)
    : MoveIterator(start),
      color(color)
{
    if (color == White) {
        direction = NE;
        moveNorthEast(current = start);
        if (current & PawnMoveTable::WhiteCaptures(start)) {
            return;
        }
        ++*this;
    } else {
        direction = SE;
        moveSouthEast(current = start);
        if (current & PawnMoveTable::BlackCaptures(start)) {
            return;
        }
        ++*this;
    }
}

PawnCaptureIterator &PawnCaptureIterator::operator++()
{
    switch (direction) {
        case NE:
            direction = NW;
            moveNorthWest(current = start);
            if (current & PawnMoveTable::WhiteCaptures(start)) {
                break;
            }
        case SE:
            direction = SW;
            moveSouthWest(current = start);
            if (current & PawnMoveTable::BlackCaptures(start)) {
                break;
            }
        case NW:
        case SW:
        default:
         current = MoveIteratorEnd;
    }
    return *this;
}

KnightIterator::KnightIterator(const u64 &start)
    : MoveIterator(start)
{
    direction = MoveIterator::NE;
    current = start;
    move2North1East(current);
    if (!(current & KnightMoveTable::NEMoves(start))) {
        ++*this;
    }
}

KnightIterator &KnightIterator::operator++() {
    u64 moves;
    u64 temp = current;
    switch (direction) {
        case MoveIterator::NE:
            moves = KnightMoveTable::NEMoves(start);
            if (moves & move1North2East(current = start)) {
                if (current != temp) {
                    break;
                }
            }
            direction = MoveIterator::SE;
            moves = KnightMoveTable::SEMoves(start);
            if (moves & move2South1East(current = start)) {
                break;
            }
        case MoveIterator::SE:
            moves = KnightMoveTable::SEMoves(start);
            if (moves & move1South2East(current = start)) {
                if (current != temp) {
                    break;
                }
            }
            direction = MoveIterator::SW;
            moves = KnightMoveTable::SWMoves(start);
            if (moves & move2South1West(current = start)) {
                break;
            }
        case MoveIterator::SW:
            moves = KnightMoveTable::SWMoves(current = start);
            if (moves & move1South2West(current = start)) {
                if (current != temp) {
                    break;
                }
            }
            direction = MoveIterator::NW;
            moves = KnightMoveTable::NWMoves(start);
            if (moves & move2North1West(current = start)) {
                break;
            }
        case MoveIterator::NW:
            moves = KnightMoveTable::NWMoves(start);
            if (moves & move1North2West(current = start)) {
                if (current != temp) {
                    break;
                }
            }
            direction = MoveIterator::END;
            current = MoveIteratorEnd;
            break;
        default:
            direction = MoveIterator::END;
            current = MoveIteratorEnd;
    }
    return *this;
}

BishopIterator::BishopIterator(const u64 &start)
    : MoveIterator(start)
{
    direction = MoveIterator::NE;
    moves = BishopMoveTable::NEMoves(start);
    if (!(moveNorthEast(current = start) & moves)) {
        ++*this;
    }
}

BishopIterator &BishopIterator::operator++()
{
    switch (direction) {
        case MoveIterator::NE:
            if (moves & moveNorthEast(current)) {
                break;
            } else {
               return nextDirection();
            }
        case MoveIterator::SE:
            if (moves & moveSouthEast(current)) {
                break;
            } else {
                return nextDirection();
             }
        case MoveIterator::SW:
            if (moves & moveSouthWest(current)) {
                break;
            } else {
                return nextDirection();
             }
        case MoveIterator::NW:
            if (moves & moveNorthWest(current)) {
                break;
            }
        default:
            direction = MoveIterator::END;
            current = MoveIteratorEnd;
    }
    return *this;
}

BishopIterator &BishopIterator::nextDirection()
{
    switch (direction) {
        case MoveIterator::NE:
            moves = BishopMoveTable::SEMoves(start);
            direction = MoveIterator::SE;
            if (moves) {
                moveSouthEast(current = start);
                break;
            } else {
                return nextDirection();
            }
        case MoveIterator::SE:
            moves = BishopMoveTable::SWMoves(start);
            direction = MoveIterator::SW;
            if (moves) {
                moveSouthWest(current = start);
                break;
            } else {
                return nextDirection();
            }
        case MoveIterator::SW:
            moves = BishopMoveTable::NWMoves(start);
            direction = MoveIterator::NW;
            if (moves) {
                moveNorthWest(current = start);
                break;
            } else {
                return nextDirection();
            }
        default:
            direction = MoveIterator::END;
            current = MoveIteratorEnd;
    }
    return *this;
}

RookIterator::RookIterator(const u64 &start)
    : MoveIterator(start)
{
    direction = MoveIterator::N;
    current = start << 8 ;
    moves = RookMoveTable::NMoves(start);
    if (!(current & moves)) {
        ++*this;
    }
}

RookIterator &RookIterator::operator++()
{
    switch (direction) {
        case MoveIterator::N:
            if (moves & moveNorth(current)) {
                break;
            } else {
               return nextDirection();
            }
        case MoveIterator::E:
            if (moves & moveEast(current)) {
                break;
            } else {
                return nextDirection();
             }
        case MoveIterator::S:
            if (moves & moveSouth(current)) {
                break;
            } else {
                return nextDirection();
             }
        case MoveIterator::W:
            if (moves & moveWest(current)) {
                break;
            }
        default:
            direction = MoveIterator::END;
            current = MoveIteratorEnd;
    }
    return *this;
}

RookIterator &RookIterator::nextDirection()
{
    switch (direction) {
        case MoveIterator::N:
            moves = RookMoveTable::EMoves(start);
            direction = MoveIterator::E;
            if (moves) {
                moveEast(current = start);
                break;
            } else {
                return nextDirection();
            }
        case MoveIterator::E:
            moves = RookMoveTable::SMoves(start);
            direction = MoveIterator::S;
            if (moves) {
                moveSouth(current = start);
                break;
            } else {
                return nextDirection();
            }
        case MoveIterator::S:
            moves = RookMoveTable::WMoves(start);
            direction = MoveIterator::W;
            if (moves) {
                moveWest(current = start);
                break;
            } else {
                return nextDirection();
            }
        default:
            direction = MoveIterator::END;
            current = MoveIteratorEnd;
    }
    return *this;
}

QueenIterator::QueenIterator(const u64 &start)
    : MoveIterator(start)
{
    direction = MoveIterator::N;
    moveNorth(current = start);
    moves = QueenMoveTable::NMoves(start);
    if (!(current & moves)) {
        nextDirection();
    }
}

QueenIterator &QueenIterator::operator++()
{
    switch (direction) {
        case MoveIterator::N:
            if (moves & moveNorth(current)) {
                break;
            } else {
               return nextDirection();
            }
        case MoveIterator::NE:
            if (moves & moveNorthEast(current)) {
                break;
            } else {
               return nextDirection();
            }
        case MoveIterator::E:
            if (moves & moveEast(current)) {
                break;
            } else {
                return nextDirection();
             }
        case MoveIterator::SE:
            if (moves & moveSouthEast(current)) {
                break;
            } else {
                return nextDirection();
             }
        case MoveIterator::S:
            if (moves & moveSouth(current)) {
                break;
            } else {
                return nextDirection();
             }
        case MoveIterator::SW:
            if (moves & moveSouthWest(current)) {
                break;
            } else {
                return nextDirection();
             }
        case MoveIterator::W:
            if (moves & moveWest(current)) {
                break;
            } else {
                return nextDirection();
             }
        case MoveIterator::NW:
            if (moves & moveNorthWest(current)) {
                break;
            }
        default:
            direction = MoveIterator::END;
            current = MoveIteratorEnd;
    }
    return *this;
}

QueenIterator &QueenIterator::nextDirection()
{
    switch (direction) {
        case MoveIterator::N:
            moves = QueenMoveTable::NEMoves(start);
            direction = MoveIterator::NE;
            if (moves) {
                moveNorthEast(current = start);
                break;
            } else {
                return nextDirection();
            }
        case MoveIterator::NE:
            moves = QueenMoveTable::EMoves(start);
            direction = MoveIterator::E;
            if (moves) {
                moveEast(current = start);
                break;
            } else {
                return nextDirection();
            }
        case MoveIterator::E:
            moves = QueenMoveTable::SEMoves(start);
            direction = MoveIterator::SE;
            if (moves) {
                moveSouthEast(current = start);
                break;
            } else {
                return nextDirection();
            }
        case MoveIterator::SE:
            moves = QueenMoveTable::SMoves(start);
            direction = MoveIterator::S;
            if (moves) {
                moveSouth(current = start);
                break;
            } else {
                return nextDirection();
            }
        case MoveIterator::S:
            moves = QueenMoveTable::SWMoves(start);
            direction = MoveIterator::SW;
            if (moves) {
                moveSouthWest(current = start);
                break;
            } else {
                return nextDirection();
            }
        case MoveIterator::SW:
            moves = QueenMoveTable::WMoves(start);
            direction = MoveIterator::W;
            if (moves) {
                moveWest(current = start);
                break;
            } else {
                return nextDirection();
            }
        case MoveIterator::W:
            moves = QueenMoveTable::NWMoves(start);
            direction = MoveIterator::NW;
            if (moves) {
                moveNorthWest(current = start);
                break;
            }
        default:
            direction = MoveIterator::END;
            current = MoveIteratorEnd;
    }
    return *this;
}

KingIterator::KingIterator(const u64 &start)
    : MoveIterator(start)
{
    direction = MoveIterator::N;
    moveNorth(current = start);
    if (!(current & KingMoveTable::NMoves(start))) {
        ++*this;
    }
}
KingIterator &KingIterator::operator++()
{
    if (direction != MoveIterator::END)
        nextDirection();
    return *this;
}

KingIterator &KingIterator::nextDirection()
{
    switch (direction) {
        case MoveIterator::N:
            direction = MoveIterator::NE;
            if ((current = KingMoveTable::NEMoves(start))) {
                break;
            }
        case MoveIterator::NE:
            direction = MoveIterator::E;
            if ((current = KingMoveTable::EMoves(start))) {
                break;
            }
        case MoveIterator::E:
            direction = MoveIterator::SE;
            if ((current = KingMoveTable::SEMoves(start))) {
                break;
            }
        case MoveIterator::SE:
            direction = MoveIterator::S;
            if ((current = KingMoveTable::SMoves(start))) {
                break;
            }
        case MoveIterator::S:
            direction = MoveIterator::SW;
            if ((current = KingMoveTable::SWMoves(start))) {
                break;
            }
        case MoveIterator::SW:
            direction = MoveIterator::W;
            if ((current = KingMoveTable::WMoves(start))) {
                break;
            }
        case MoveIterator::W:
            direction = MoveIterator::NW;
            if ((current = KingMoveTable::NWMoves(start))) {
                break;
            }
        default:
            direction = MoveIterator::END;
            current = MoveIteratorEnd;
            break;
    }
    return *this;
}
