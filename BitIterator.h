 #ifndef BITITERATOR_H
#define BITITERATOR_H

#include "Common.h"

class BitIterator;
class Direction;


class MoveIterator {
public:
    enum Direction {
        N        = 0x01,
        NE       = 0x02,
        E        = 0x04,
        SE       = 0x08,
        S        = 0x10,
        SW       = 0x20,
        W        = 0x40,
        NW       = 0x80,
        END      = 0x00,
        DIAGONAL = NE | SE | SW | NW,
        STRAIGHT = N  | S  | E  | W,
        UPPER    = W  | NW | N  | NE,
        LOWER    = E  | SE | S  | SW,
    };
    u64 operator*();
    operator bool();
    static const MoveIterator& end();
    template <typename MoveIteratorish>
    friend bool operator==(MoveIteratorish const& lhs, MoveIteratorish const& rhs) {
        return lhs.current == rhs.current;
    }
    template <typename MoveIteratorish>
    friend bool operator!=(MoveIteratorish const& lhs, MoveIteratorish const& rhs) {
        return lhs.current != rhs.current;

    }
    u64 const& getMoves() const;
protected:
    MoveIterator(u64 const& start);
    const u64 start;
    Direction direction;
    u64 current;
    u64 moves;
private:
    MoveIterator();
};

class DirectionIterator : public MoveIterator {
public:
    DirectionIterator(u64 const &start, u64 const& directionGuide);
    DirectionIterator(u64 const &start, Direction direction);
    DirectionIterator& operator++();
    static DirectionIterator end();
    bool isDiagonal();
    u64 getMoves();

private:
    u64 moves;

};

class PawnMoveIterator : public MoveIterator {
public:
    PawnMoveIterator(u64 const& start, PieceColor color);
    PawnMoveIterator& operator++();
private:
    PieceColor color;
};

class PawnCaptureIterator : public MoveIterator {
public:
    PawnCaptureIterator(u64 const& start, PieceColor color);
    PawnCaptureIterator& operator++();
private:
    PieceColor color;
};

class KnightIterator : public MoveIterator {
public:
    KnightIterator(u64 const& start);
    KnightIterator& operator++();
};

class BishopIterator : public MoveIterator {
public:
    BishopIterator(u64 const& start);
    BishopIterator& operator++();
    BishopIterator& nextDirection();
};

class RookIterator : public MoveIterator {
public:
    RookIterator(u64 const& start);
    RookIterator& operator++();
    RookIterator& nextDirection();
};

class QueenIterator : public MoveIterator {
public:
    QueenIterator(u64 const& start);
    QueenIterator& operator++();
    QueenIterator& nextDirection();
};

class KingIterator : public MoveIterator {
public:
    KingIterator(u64 const& start);
    KingIterator& operator++();
    KingIterator& nextDirection();
};

class BitIterator {
public:
    BitIterator(u64 const& container);
    BitIterator& operator++();
    u64 operator*();
    operator bool();
    friend bool operator==(BitIterator const& lhs, BitIterator const& rhs);
    friend bool operator!=(BitIterator const& lhs, BitIterator const& rhs);
private:
    u64 current;
    u64 container;
};

#endif // BITITERATOR_H
