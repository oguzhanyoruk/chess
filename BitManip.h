#ifndef BITMANIP_H
#define BITMANIP_H

#include "Masks.h"

namespace bitmanip {
int setBitCount(u64 const& t);
}

#endif // BITMANIP_H
