#include "BitIterator.h"
#include "BitManip.h"
#include "Board.h"
#include "MoveLookupTable.h"
#include <QDebug>


#define isSameColorSquare(s1, s2)  static_cast<bool>(s1 & mask::board::white) && static_cast<bool>(s2 & mask::board::white)

#define SquareColor(s) (s & mask::board::white ? mask::board::white : mask::board::black)

#define OppositeSquareColor(s) (s & mask::board::white ? mask::board::black : mask::board::white)

auto static end = MoveIterator::end();


class MoveCheck {
public:
    static u64 whitePawnMoves(u64 const& from, u64 const& friends, u64 const& enemies, u8 const& special) {
        u64 moves = 0x0;
        auto pmi = PawnMoveIterator(from, White);
        while (pmi != end) {
            if (*pmi & (enemies | friends)) {
                break;
            }
            moves |= *pmi;
            ++pmi;
        }
        u64 enpassant = mask::rank::_5 & from ? (mask::special::doublePawn::multiplier * special) : 0x0;
        moves |= (PawnMoveTable::WhiteCaptures(from) & (enemies | enpassant));
        return moves;
    }
    static u64 blackPawnMoves(u64 const& from, u64 const& friends, u64 const& enemies, u8 const& special) {
        u64 moves = 0x0;
        auto pmi = PawnMoveIterator(from, Black);
        while (pmi != end) {
            if (*pmi & (enemies | friends)) {
                break;
            }
            moves |= *pmi;
            ++pmi;
        }
        u64 enpassant = mask::rank::_4 & from ? (mask::special::doublePawn::multiplier * special) : 0x0;
        moves |= (PawnMoveTable::BlackCaptures(from) & (enemies | enpassant));
        return moves;
    }

    static u64 rookMoves(u64 const& from, u64 const& friends, u64 const& enemies) {
        u64 moves = 0x0;
        auto ri = RookIterator(from);
        while (ri != end) {
            if (*ri) {
                if (*ri & enemies) {
                    moves |= *ri;
                    ri.nextDirection();
                } else if (*ri & friends) {
                    ri.nextDirection();
                } else {
                    moves |= *ri;
                    ++ri;
                }
            } else {
                ++ri;
            }
        }
        return moves;
    }
    static u64 knightMoves(u64 const& from, u64 const& friends) {
        u64 moves = 0x0;
        auto ki = KnightIterator(from);
        while (ki != end) {
            if (*ki && !(*ki & friends)) {
                moves |= *ki;
            }
            ++ki;
        }
        return moves;
    }
    static u64 bishopMoves(u64 const& from, u64 const& friends, u64 const& enemies) {
        u64 moves = 0x0;
        auto bi = BishopIterator(from);
        while (bi != end) {
            if (*bi) {
                if (*bi & enemies) {
                    moves |= *bi;
                    bi.nextDirection();
                } else if (*bi & friends) {
                    bi.nextDirection();
                } else {
                    moves |= *bi;
                    ++bi;
                }
            } else {
                ++bi;
            }
        }
        return moves;
    }
    static u64 queenMoves(u64 const& from, u64 const& friends, u64 const& enemies) {
        u64 moves = 0x0;
        auto qi = QueenIterator(from);
        while (qi != end) {
            if (*qi) {
                if (*qi & enemies) {
                    moves |= *qi;
                    qi.nextDirection();
                } else if (*qi & friends) {
                    qi.nextDirection();
                } else {
                    moves |= *qi;
                    ++qi;
                }
            } else {
                ++qi;
            }
        }
        return moves;
    }

    static u64 kingMoves(u64 const& from, u64 const& friends, u64 const& enemyKing) {
        u64 moves = 0x0;
        const u64 forbidden = friends | enemyKing;
        auto ki = KingIterator(from);
        while (ki != end) {
            if (*ki && !(*ki & forbidden)) {
                moves |= *ki;
            }
            ++ki;
        }
        return moves;
    }
};


Board::Board() noexcept
{
    reset();
}

CheckStatus Board::inCheck(PieceColor color) const
{
    CheckStatus status;
    u64 attacking, defending;
    getPieces(!color, attacking, defending);
    u64 defendingKing = kings & defending;
    u64 pieces = attacking | defending;
    //Check for diagonal attacks
    {
        auto bi = BishopIterator(defendingKing);
        if (bi != end) {
            u64 kingSquareColor = SquareColor(defendingKing);
            u64 diagonalAttackers = (attacking & (queens | bishops)) & kingSquareColor;
            if (diagonalAttackers) {
                while (bi != end) {
                    if (*bi) {
                        if (*bi & diagonalAttackers) {
                            status.addCheckingPiece(*bi);
                            bi.nextDirection();
                            continue;
                        } else if (*bi & pieces) {
                            bi.nextDirection();
                            continue;
                        }

                    }
                    ++bi;
                }
            }
        }
    }
    {
        auto ni = KnightIterator(defendingKing);
        if (ni != end) {
            u64 attackingKnights = (attacking & knights) & OppositeSquareColor(defendingKing);
            if (attackingKnights) {
                while (ni != end) {
                    if (*ni & attackingKnights) {
                        status.addCheckingPiece(*ni);
                    }
                    ++ni;
                }
            }
        }
    }
    {
        auto ri = RookIterator(defendingKing);
        if (ri != end) {
            auto straightAttackers = (attacking & (queens | rooks));
            if (straightAttackers) {
                while (ri != end) {
                    if (*ri) {
                        if (*ri & straightAttackers) {
                            status.addCheckingPiece(*ri);
                            ri.nextDirection();
                            continue;
                        } else if (*ri & pieces) {
                            ri.nextDirection();
                            continue;
                        }
                    }
                    ++ri;
                }
            }
        }
    }
    {
        auto pci = PawnCaptureIterator(defendingKing, color);
        if (pci != end) {
            auto attackingPawns = pawns & attacking;
            while (pci != end) {
                if (*pci & attackingPawns) {
                    status.addCheckingPiece(*pci);
                }
                ++pci;
            }
        }
    }
    {
        auto ki = KingIterator(defendingKing);
        if (ki != end) {
            auto attackingKing = kings & attacking;
            while (ki != end) {
                if (*ki & attackingKing) {
                    status.addCheckingPiece(*ki);
                }
                ++ki;
            }
        }
    }
    return status;
}

bool Board::isLegal(const u64 &from, const u64 &to) const
{
    PieceColor color;
    if (from & whites) {
        color = White;
    } else if (from & blacks) {
        color = Black;
    } else {
        return false;
    }
    u64 friends;
    u64 enemies;
    getPieces(color, friends, enemies);
    if (from & pawns) {
        return true;
    } else if (from & rooks) {
        return to & MoveCheck::rookMoves(from, friends, enemies);
    } else if (from & knights) {
        return to & MoveCheck::knightMoves(from, friends);
    } else if (from & bishops) {
        return to & MoveCheck::bishopMoves(from, friends, enemies);
    } else if (from & queens) {
        return to & MoveCheck::queenMoves(from, friends, enemies);
    } else if (from & kings) {
        return to & MoveCheck::kingMoves(from, friends, kings & enemies);
    }
    return false;
}

PieceColor Board::getColor(const u64 &at) const
{
    return at & whites;
}

u64 Board::legalMoves(const u64 &at) const {
    PieceColor color;
    if (at & whites) {
        color = White;
    } else if (at & blacks) {
        color = Black;
    } else {
        return 0x0;
    }
    u64 friends;
    u64 enemies;
    getPieces(color, friends, enemies);
    const u64 pinInfo = getAbsolutePinInfo(at);
    const CheckStatus status = inCheck(color);
    u64 checkMask = 0x0;
    if (at & kings) {
        u64 moves = MoveCheck::kingMoves(at, friends, enemies & kings);
        if (!status.isChecked) {
             moves |= (((0xFFFFFFFFFFFFFFFF * static_cast<u64>(canShortCastle(color))) & (at >> 2)) |
             ((0xFFFFFFFFFFFFFFFF * static_cast<u64>(canLongCastle(color))) & (at << 2)));
        }
        auto ki = KingIterator(at);
        while (ki != end) {
            if (*ki & moves) {
                Board c = clone();
                c.doMove(at, *ki);
                if (c.inCheck(color).isChecked) {
                    moves ^= *ki;
                }
            }
            ++ki;
        }
        return moves;
    }
    if (status.isChecked) {
        if (!(at & kings) && status.isDoubleChecked) {
            return 0x0;
        }
        checkMask = status.checkingPieces;
        if (!(status.checkingPieces & knights) && !(status.checkingPieces & pawns)) {
            DirectionIterator di(friends & kings, status.checkingPieces);
            while (*di != status.checkingPieces && di != end) {
                checkMask |= *di;
                ++di;
            }
        }
    } else {
        checkMask = *end;
    }
    if (at & pawns) {
        return checkMask & pinInfo & (color ?
            MoveCheck::whitePawnMoves(at, friends, enemies, enpassant) :
            MoveCheck::blackPawnMoves(at, friends, enemies, enpassant));
    } else if (at & rooks) {
        return checkMask & pinInfo & MoveCheck::rookMoves(at, friends, enemies);
    } else if (at & knights) {
        return checkMask & pinInfo & MoveCheck::knightMoves(at, friends);
    } else if (at & bishops) {
        return checkMask & pinInfo & MoveCheck::bishopMoves(at, friends, enemies);
    } else if (at & queens) {
        return checkMask & pinInfo & MoveCheck::queenMoves(at, friends, enemies);
    }
    return 0x0;
}

bool Board::move(const u64 &from, const u64 &to)
{
    if (legalMoves(from) & to) {
        doMove(from, to);
        nextTurn();
        return true;
    }
    return false;
}

//Caller should ensure that it's a legal move or else it will cause inconsistencies
void Board::moveUnsafe(const u64 &from, const u64 &to)
{
    doMove(from, to);
    nextTurn();
}

Board Board::clone() const
{
    return Board(*this);
}

Board::Status Board::getStatus()
{
    return handleStatus();
}

BitIterator Board::attackingPieces() const
{
    return  BitIterator(status == WHITE_TURN ? whites : blacks);
}

std::vector<Move> Board::legalMoves() const
{
    u64 pieces = status == WHITE_TURN ? whites : blacks;
    int count = 0;
    {
        BitIterator pi(pieces);
        while (pi) {
            count += bitmanip::setBitCount(legalMoves(*pi));
            ++pi;
        }
    }
    BitIterator pi(pieces);
    std::vector<Move> moves;
    moves.reserve(count);
    while (pi) {
        auto lm = legalMoves(*pi);
        BitIterator bi(lm);
        while (bi) {
            moves.emplace_back(*pi, *bi);
            ++bi;
        }
        ++pi;
    }
    return moves;
}

std::vector<Move> Board::possibleOpponentMoves() const
{
    u64 pieces = status == WHITE_TURN ? blacks : whites;
    int count = 0;
    {
        BitIterator pi(pieces);
        while (pi) {
            count += bitmanip::setBitCount(legalMoves(*pi));
            ++pi;
        }
    }
    BitIterator pi(pieces);
    std::vector<Move> moves;
    moves.reserve(count);
    while (pi) {
        auto lm = legalMoves(*pi);
        BitIterator bi(lm);
        while (bi) {
            moves.emplace_back(*pi, *bi);
            ++bi;
        }
        ++pi;
    }
    return moves;
}

int Board::legalMovesCount() const
{
    u64 pieces = status == WHITE_TURN ? whites : blacks;
    int count = 0;
    BitIterator i(pieces);
    while (i) {
        count += bitmanip::setBitCount(legalMoves(*i));
        ++i;
    }
    return count;
}

void Board::getPieces(PieceColor friendColor, u64 &friends, u64 &enemies) const
{
    if (friendColor) {
        friends = whites;
        enemies = blacks;
    } else {
        friends = blacks;
        enemies = whites;
    }
}

bool Board::isThreatenedBy(const u64 &threatened, PieceColor threatheningColor) const
{
    u64 attacking, defending;
    getPieces(threatheningColor, attacking, defending);
    {
        auto bi = BishopIterator(threatened);
        if (bi != end) {
            u64 kingSquareColor = SquareColor(threatened);
            u64 diagonalAttackers = (attacking & (queens | bishops)) & kingSquareColor;
            if (diagonalAttackers) {
                while (bi != end) {
                    if (*bi) {
                        if (*bi & diagonalAttackers) {
                            return true;
                        } else if (*bi & defending) {
                            bi.nextDirection();
                            continue;
                        }

                    }
                    ++bi;
                }
            }
        }
    }
    {
        auto ni = KnightIterator(threatened);
        if (ni != end) {
            u64 attackingKnights = (attacking & knights) & OppositeSquareColor(threatened);
            if (attackingKnights) {
                while (ni != end) {
                    if (*ni & attackingKnights) {
                        return true;
                    }
                    ++ni;
                }
            }
        }
    }
    {
        auto ri = RookIterator(threatened);
        if (ri != end) {
            auto straightAttackers = (attacking & (queens | rooks));
            if (straightAttackers) {
                while (ri != end) {
                    if (*ri) {
                        if (*ri & straightAttackers) {
                            return true;
                        } else if (*ri & defending) {
                            ri.nextDirection();
                            continue;
                        }
                    }
                    ++ri;
                }
            }
        }
    }
    {
        auto pci = PawnCaptureIterator(threatened, !threatheningColor);
        if (pci != end) {
            auto attackingPawns = pawns & attacking;
            while (pci != end) {
                if (*pci & attackingPawns) {
                    return true;
                }
                ++pci;
            }
        }
    }
    {
        auto ki = KingIterator(threatened);
        if (ki != end) {
            auto attackingKing = kings & attacking;
            while (ki != end) {
                if (*ki & attackingKing) {
                    return true;
                }
                ++ki;
            }
        }
    }
    return false;
}

bool Board::canShortCastle(PieceColor color) const
{
    u8 shortCastleCheck = color ? mask::special::castle::white::checkShort : mask::special::castle::black::checkShort;
    //Check if rook or king has moved
    if (castle & shortCastleCheck) {
        return false;
    }
    u64 friends, enemies;
    getPieces(color, friends, enemies);
    u64 king = kings & friends;
    u64 friendRooks = rooks & friends;
    u64 all = friends | enemies;
    auto di = DirectionIterator(king, DirectionIterator::E);
    //Check if the sqaures in between are occupied and also if it is being threatened
    while (!(*di & friendRooks) && di != end) {
        if (all & *di) {
            return false;
        }
        ++di;
    }
    if (isThreatenedBy(king >> 1, !color) || isThreatenedBy(king >> 2, !color)) {
        return false;
    }
    return true;
}

bool Board::canLongCastle(PieceColor color) const
{
    u8 longCastleCheck = color ? mask::special::castle::white::checkLong : mask::special::castle::black::checkLong;
    //Check if rook or king has moved
    if (castle & longCastleCheck) {
        return false;
    }
    u64 friends, enemies;
    getPieces(color, friends, enemies);
    u64 king = kings & friends;
    u64 friendRooks = rooks & friends;
    u64 all = friends | enemies;
    auto di = DirectionIterator(king, DirectionIterator::W);
    //Check if the sqaures in between are occupied and also if it is being threatened
    while (!(*di & friendRooks) && di != end) {
        if (all & *di) {
            return false;
        }
        ++di;
    }
    if (isThreatenedBy(king << 1, !color) || isThreatenedBy(king << 2, !color)) {
        return false;
    }
    return true;
}

u64 Board::getAbsolutePinInfo(const u64 &piece) const
{
    PieceColor color = whites & piece;
    u64 attacking, defending;
    getPieces(color, defending, attacking);
    u64 defendingKing = kings & defending;
    DirectionIterator di(defendingKing, piece);
    if (!*di) {
        return mask::board::full;
    }
    u64 pinInfo = 0x0;
    while (*di != piece) {
        if (*di & (defending | attacking)) {
            return mask::board::full;
        }
        pinInfo |= *di;
        ++di;
    }
    ++di;
    u64 threats = di.isDiagonal() ? attacking & (bishops | queens) : attacking & (rooks | queens);
    while (di != DirectionIterator::end()) {
        if (*di) {
            if (*di & defending) {
                return mask::board::full;
            }
            else if (*di & threats) {
                return pinInfo |= *di;
            }
            else if (*di & attacking) {
                return mask::board::full;
            }
            pinInfo |= *di;
            ++di;
        }
    }
    return mask::board::full;
}

void Board::doMove(const u64 &from, const u64 &to)
{
    handleEnpassant(from, to);
    handleCastle(from, to);
    kings       &= ~to;
    queens      &= ~to;
    bishops     &= ~to;
    knights     &= ~to;
    rooks       &= ~to;
    pawns       &= ~to;
    whites      &= ~to;
    blacks      &= ~to;
    kings        = kings & from ? (kings & ~from) | to : kings;
    queens       = queens & from ? (queens & ~from) | to : queens;
    bishops      = bishops & from ? (bishops & ~from) | to : bishops;
    knights      = knights & from ? (knights & ~from) | to : knights;
    rooks        = rooks & from ? (rooks & ~from) | to : rooks;
    if (isPromotion(from, to)) {
        pawns   &= ~from;
        switch (promoteTo()) {
           case Queen:
                queens |= to;
                break;
            case Rook:
                rooks  |= to;
                break;
            case Bishop:
                bishops |= to;
                break;
            case Knight:
                knights |= to;
                break;
            default:
                queens |= to;
                break;
        }

    } else {
        pawns    = pawns & from ? (pawns & ~from) | to : pawns;
    }
    whites       = whites & from ? (whites & ~from) | to : whites;
    blacks       = blacks & from ? (blacks & ~from) | to : blacks;

}

void Board::handleCastle(const u64 &from, const u64 &to)
{
    PieceColor color = from & whites;
    if (from & kings) {
        if ((from << 2) == to) {
            rooks &= ~(from << 4);
            if (color) {
                whites &= ~(from << 4);
                whites |= (from << 1);
                castle |= mask::special::rookMoves::longSide::white;
            } else {
                blacks &= ~(from << 4);
                blacks |= (from << 1);
                castle |= mask::special::rookMoves::longSide::black;
            }
            rooks |= (from << 1);
        } else if ((from >> 2) == to) {
            rooks &= ~(from >> 3);
            if (color) {
                whites &= ~(from >> 3);
                whites |= (from >> 1);
                castle |= mask::special::rookMoves::shortSide::white;
            } else {
                blacks &= ~(from >> 3);
                blacks |= (from >> 1);
                castle |= mask::special::rookMoves::shortSide::black;
            }
            rooks |= (from >> 1);
        }
        if (color) {
            castle |= mask::special::kingMoves::white;
        } else {
            castle |= mask::special::kingMoves::black;
        }
        return;
    }
    if (from & whites & mask::square::a1 & rooks) {
        castle |= mask::special::rookMoves::longSide::white;
    } else if (from & whites & mask::square::h1 & rooks) {
        castle |= mask::special::rookMoves::shortSide::white;
    } else if (from & blacks & mask::square::a8 & rooks) {
        castle |= mask::special::rookMoves::longSide::black;
    } else if (from & blacks & mask::square::h8 & rooks) {
        castle |= mask::special::rookMoves::shortSide::black;
    }
}

bool Board::isPromotion(const u64 &from, const u64 &to) const
{
    if (from & mask::rank::_7 && to & mask::rank::_8 && from & pawns) {
        return true;
    } else if (from & mask::rank::_2 && to & mask::rank::_1 && from & pawns) {
        return true;
    } else {
        return false;
    }
}


void Board::handleEnpassant(const u64& from, const u64& to) {
    if (!(from & pawns)) {
        enpassant = 0x0;
        return;
    }
    if ((from & whites)) {
        if ((mask::rank::_2 & from) && (mask::rank::_4 & to)) {
            enpassant = static_cast<u8>(from >> 8);
            return;
        }
        else if (mask::rank::_5 & from && (to & mask::special::doublePawn::multiplier * enpassant)) {
           pawns &= ~(to >> 8);
           blacks &= ~(to >> 8);
        }
    }
    else if ((from & blacks)) {
        if ((mask::rank::_7 & from) && (mask::rank::_5 & to)) {
            enpassant = static_cast<u8>(from >> 48);
            return;
        }
        else if (mask::rank::_4 & from && (to & mask::special::doublePawn::multiplier * enpassant)) {
            pawns &= ~(to << 8);
            whites &= ~(to << 8);
        }
    }
    enpassant = 0x0;
    return;

}

void Board::nextTurn()
{
    status = static_cast<Board::Status>(!status);
}

PieceType Board::promoteTo()
{
    return Queen;
}

Board::Status Board::handleStatus()
{
    if (status == WHITE_TURN) {
        BitIterator i(whites);
        while (i) {
            if (legalMoves(*i)) {
                return status = WHITE_TURN;
            }
            ++i;
        }
        if (inCheck(White).isChecked) {
            return status = BLACK_WON;
        }
        else {
            return status = DRAW_BY_STALEMATE;
        }
    }
    if (status == BLACK_TURN) {
        BitIterator i(blacks);
        while (i) {
            if (legalMoves(*i)) {
                return status = BLACK_TURN;
            }
            ++i;
        }
        if (inCheck(Black).isChecked) {
            return status = WHITE_WON;
        }
        else {
            return status = DRAW_BY_STALEMATE;
        }
    }
    return status;
}

void Board::reset()
{
    using namespace mask;
    kings       = square::e1 | square::e8;
    queens      = square::d1 | square::d8;
    bishops     = square::c1 | square::f1 | square::c8 | square::f8;
    knights     = square::b1 | square::g1 | square::b8 | square::g8;
    rooks       = square::a1 | square::h1 | square::a8 | square::h8;
    pawns       = rank::_2 | rank::_7;
    whites      = rank::_1 | rank::_2;
    blacks      = rank::_7 | rank::_8;
    castle      = 0x0;
    enpassant   = 0x0;
    status = WHITE_TURN;
}
