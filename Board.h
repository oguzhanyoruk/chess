#ifndef BOARD_H
#define BOARD_H

#include "Common.h"
#include "CheckStatus.h"
#include "Move.h"

#include <memory>
#include <vector>

class Board
{
public:
    enum Status {
        WHITE_TURN,
        BLACK_TURN,
        DRAW_BY_STALEMATE,
        DRAW_BY_INSUFFICIENT_MATERIAL,
        DRAW_BY_THREEFOLD_REPETATION,
        WHITE_WON,
        BLACK_WON
    };
    Board() noexcept;
    Board(Board const&) = default;
    Board(Board&&) = default;
    Board& operator=(Board const&) = default;
    Board& operator=(Board&&) = default;
    CheckStatus inCheck(PieceColor color) const;
    bool isLegal(u64 const& from, u64 const& to) const;
    PieceColor getColor(const u64 &at) const;
    u64 legalMoves(u64 const& at) const;
    bool move(u64 const& from, u64 const& to);
    void moveUnsafe(u64 const& from, u64 const& to);
    Board clone() const;
    Status getStatus();
    BitIterator attackingPieces() const;
    std::vector<Move> legalMoves() const;
    std::vector<Move> possibleOpponentMoves() const;
    int legalMovesCount() const;
    inline u64 getKings() const {return kings;}
    inline u64 getQueens() const {return queens;}
    inline u64 getBishops() const {return bishops;}
    inline u64 getKnights() const {return knights;}
    inline u64 getRooks() const {return rooks;}
    inline u64 getPawns() const {return pawns;}
    inline u64 getWhites() const {return whites;}
    inline u64 getBlacks() const {return blacks;}
    inline u8  getCastle() const {return castle;}
    inline u8  getEnpassant() const {return enpassant;}
    friend class BoardRecorder;

private:
    void getPieces(PieceColor friendColor, u64& friends, u64& enemies) const;
    bool isThreatenedBy(u64 const& threatened, PieceColor threatheningColor) const;
    bool canShortCastle(PieceColor color) const;
    bool canLongCastle(PieceColor color) const;
    u64 getAbsolutePinInfo(u64 const& piece) const;
    void doMove(u64 const& from, u64 const& to);
    void handleCastle(u64 const& from, u64 const& to);
    bool isPromotion(u64 const& from, u64 const& to) const;
    void handleEnpassant(const u64 &from, const u64 &to);
    void nextTurn();
    PieceType promoteTo();
    Status handleStatus();
    void reset();
    u64 kings;
    u64 queens;
    u64 bishops;
    u64 knights;
    u64 rooks;
    u64 pawns;
    u64 whites;
    u64 blacks;
    u8  castle;
    u8  enpassant;
    Status status;
};

#endif // BOARD_H
