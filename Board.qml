import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.1
import Board.Blocks 1.0
import QtGraphicalEffects 1.12

Item {
    id: root
    property int side
    height: side
    width: side
    focus: true
    Keys.onRightPressed: {
        console.log("RightArrow pressed")
        boardModel.redo()
    }
    Keys.onLeftPressed: {
        console.log("LeftArrow pressed")
        boardModel.undo()
    }

    Component.onCompleted: {
        boardModel.nextTurn()
    }

    readonly property var fileMap: {
        0: "a",
        1: "b",
        2: "c",
        3: "d",
        4: "e",
        5: "f",
        6: "g",
        7: "h",
    }
    readonly property var rankMap: {
        0: "8",
        1: "7",
        2: "6",
        3: "5",
        4: "4",
        5: "3",
        6: "2",
        7: "1",
    }

    Component {
        id: squareWithPieceDelegate
        DropArea {
            keys: ["pieceMoving"]
            width: board.cellWidth
            height: board.cellHeight
            property int index: itemSquareIndex
            Square {
                length: board.cellWidth
                color: itemSquareColor
                Text {
                   visible: itemSquareIndex % 8 == 7
                   anchors.right: parent.right
                   anchors.top: parent.top
                   color: "black"
                   text: root.rankMap[Math.floor(itemSquareIndex / 8)]
                   anchors.margins: 5
                }
                Text {
                   visible: Math.floor(itemSquareIndex / 8) == 7
                   anchors.right: parent.right
                   anchors.bottom: parent.bottom
                   color: "black"
                   text: root.fileMap[itemSquareIndex % 8]
                   anchors.margins: 5
                }
            }
            MouseArea {
                id: mouseArea
                anchors.fill: parent
                anchors.centerIn: parent
                drag.target: pieceImage
                property int previousIndex: parent.index
                onPressed: {
                    previousIndex = parent.index
                    boardModel.setPieceSelected(previousIndex)
                }
                enabled: itemImageSource !== undefined

                onReleased: {
                    var previousParent = parent
                    var p = pieceImage.Drag.target !== null ? pieceImage.Drag.target : parent
                    if (previousIndex !== p.index) {
                        if (boardModel.pieceMove(previousIndex, p.index)) {
                            boardModel.resetPieceSelected();
                            boardModel.nextTurn();
                        }

                    }
                }
                Image {
                    id: pieceImage
                    source: itemImageSource === undefined ? "": itemImageSource

                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    Drag.keys: [ "pieceMoving" ]
                    Drag.active: mouseArea.drag.active
                    Drag.hotSpot.x: width / 2
                    Drag.hotSpot.y: height / 2
                    Drag.onDragFinished: {
                        console.log("Drag finished")
                    }

                    states: State {
                        when: mouseArea.drag.active
                        ParentChange { target: pieceImage; parent: root }

                        AnchorChanges { target: pieceImage; anchors.verticalCenter: undefined; anchors.horizontalCenter: undefined }
                    }

                }
            }
        }

    }

    Dialog {
        id: dialog
        modal: true
        property string text
        standardButtons: Dialog.Ok
        visible: false
        Text {
            id: gameEndText
            text: dialog.text
        }
    }
    Connections {
        target: boardModel
        function onDraw() {
            dialog.text = "Draw"
            dialog.visible = true
        }
        function onWhiteWon() {
            dialog.text = "White has won!"
            dialog.visible = true
        }
        function onBlackWon() {
            dialog.text = "Black has won!"
            dialog.visible = true
        }
    }

    GridView {
        id: board
        anchors.fill: parent
        cellWidth: width / 8
        cellHeight: height / 8
        model: boardModel
        boundsMovement: Flickable.StopAtBounds
        delegate: Loader {
            id: loader
            property var itemSquareColor: squareColor
            property var itemImageSource: imageSource
            property var itemSquareIndex: index
            property var itemHighlight  : highlight
            sourceComponent: squareWithPieceDelegate
            RectangularGlow {
                parent: loader
                anchors.fill: loader.item
                glowRadius: 0
                cornerRadius: 0
                spread: 0
                color: itemHighlight
                z: loader.item.z + 1
            }
        }
    }
}

