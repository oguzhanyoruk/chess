#include "BoardModel.h"
#include "Square.h"
#include "MoveLookupTable.h"

#include <set>

static std::set<int> changedIndexes(Board const& previous, Board const& current) {
    u64 previousWhites = previous.getWhites();
    u64 previousBlacks = previous.getBlacks();
    u64 currentWhites = current.getWhites();
    u64 currentBlacks = current.getBlacks();
    BitIterator white(currentWhites ^ previousWhites);
    BitIterator black(currentBlacks ^ previousBlacks);
    std::set<int> indexes;
    while (white) {
        indexes.insert(mlt::index(*white));
        ++white;
    }
    while (black) {
        indexes.insert(mlt::index(*black));
        ++black;
    }
    return indexes;
}

static Square::Color indexToColor(int index) {
    return  (1ULL << (63 - index) & mask::board::white) ? Square::Color::Light : Square::Color::Dark;
}

const QColor greenHighlight = "#2000ff00";
const QColor redHighlight   = "#20ff0000";
const QColor noHighlight    = "transparent";

static const std::vector<const char*> files = {
    "a", "b", "c", "d", "e", "f", "g", "h",
};

static const std::vector<const char*> ranks = {
    "1", "2", "3", "4", "5", "6", "7", "8",
};

QString indexToHumanReadable(int index) {
    auto ri = (63 - index) / 8;
    auto fi = 7 - (63 - index) % 8;
    return QString() + files[fi] + ranks[ri];
}

static QVariant toImageSource(Board const& board, int index) {
    u64 coordinate = 1ULL << (63 - index);
    QString imageSource = "";
    if (coordinate & board.getWhites()) {
        imageSource += "/pieces/white_";
    } else if (board.getBlacks() & coordinate) {
        imageSource += "/pieces/black_";
    } else {
        return imageSource;
    }
    if (coordinate & board.getPawns()) {
        imageSource += "pawn";
    } else if (coordinate & board.getRooks()) {
        imageSource += "rook";
    } else if (coordinate & board.getKnights()) {
        imageSource += "knight";
    } else if (coordinate & board.getBishops()) {
        imageSource += "bishop";
    } else if (coordinate & board.getQueens()) {
        imageSource += "queen";
    } else if (coordinate & board.getKings()) {
        imageSource += "king";
    } else {
        qDebug() << "Unexpected case!";
    }
    return imageSource;
}

BoardModel::BoardModel(QObject *parent)
    : QAbstractListModel(parent),
      board(),
      legalMoves(0x0),
      turn(White),
      recorder()
{
    _roleNames = {
        {Roles::SquareColor, "squareColor"},
        {Roles::Highlight,   "highlight"},
        {Roles::ImageSource, "imageSource"},
    };
    recorder.record(board);
}

int BoardModel::rowCount(const QModelIndex&) const
{
    return 64;
}

QVariant BoardModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }
    if (role == Roles::SquareColor) {
        return indexToColor(index.row());
    } else if (role  == Roles::ImageSource) {
        return toImageSource(board, index.row());;
    } else if (role == Roles::Highlight) {
        if (selectedIndex < 0) {
            return noHighlight;
        }
        if (legalMoves & (1ULL << (63 - index.row()))) {
            return greenHighlight;
        }
        else {
            return noHighlight;
        }
    }
    return QVariant();
}

QHash<int, QByteArray> BoardModel::roleNames() const
{
    return _roleNames;
}

bool BoardModel::pieceMove(int previousIndex, int currentIndex)
{
    u64 const from = 1ULL << (63 - previousIndex);
    if (board.getColor(from) != turn) {
        return false;
    }
    u64 const to = 1ULL << (63 - currentIndex);
    Board previous = board.clone();
    if (board.move(from, to)) {
        emit textChanged(indexToHumanReadable(previousIndex) + "->" + indexToHumanReadable(currentIndex));
        notifyChanged(changedIndexes(previous, board));
        recorder.record(board);
        setStatus();
        return true;
    }
    return false;
}

int BoardModel::legalMovesCount()
{
    return board.legalMovesCount();
}

void BoardModel::setPieceSelected(int selected)
{
    if (selectedIndex == selected)
        return;
    u64 at = 1ULL << (63 - selected);
    selectedIndex = selected;
    if(turn == board.getColor(at)) {
        legalMoves = board.legalMoves(at);
    } else {
        legalMoves = 0x0;
    }

    emit dataChanged(index(0), index(63), {Roles::Highlight});
}

void BoardModel::resetPieceSelected()
{
    selectedIndex = -1;
    legalMoves = 0x0;
    emit dataChanged(index(0), index(63), {Roles::Highlight});
}

void BoardModel::undo()
{
    auto previous = board.clone();
    board = recorder.undo();
    setStatus();
    notifyChanged(changedIndexes(previous, board));;
}

void BoardModel::redo()
{
    auto previous = board.clone();
    board = recorder.redo();
    setStatus();
    notifyChanged(changedIndexes(previous, board));
}

BoardModel::~BoardModel()
{

}

void BoardModel::notifyChanged(const std::set<int> &changed)
{
    for (auto i : changed) {
        emit dataChanged(index(i), index(i), {Roles::ImageSource});
    }
}

void BoardModel::setStatus()
{
    Board::Status status = board.getStatus();
    if (status == Board::WHITE_TURN) {
        turn = White;
    } else if (status == Board::BLACK_TURN) {
        turn = Black;
    } else if (status == Board::DRAW_BY_STALEMATE) {
        emit draw();
    } else if (status == Board::WHITE_WON) {
        emit whiteWon();
    } else if (status == Board::BLACK_WON) {
        emit blackWon();
    }
    emit legalMovesCountChanged();
}
