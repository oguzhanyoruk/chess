#ifndef BOARDMODEL_H
#define BOARDMODEL_H

#include <QAbstractListModel>

#include "BoardRecorder.h"

#include <set>

struct Piece;

class BoardModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit BoardModel(QObject* parent = nullptr);
    enum Roles {
        SquareColor = Qt::UserRole + 1,
        Highlight,
        ImageSource
    };
    Q_ENUM(Roles)
public:
    int rowCount(const QModelIndex &) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;
    Q_INVOKABLE bool pieceMove(int previousIndex, int currentIndex);
    Q_PROPERTY(int legalMovesCount READ legalMovesCount NOTIFY legalMovesCountChanged)
    Q_INVOKABLE int legalMovesCount();
    Q_INVOKABLE void setPieceSelected(int index);
    Q_INVOKABLE void resetPieceSelected();
    Q_INVOKABLE void undo();
    Q_INVOKABLE void redo();
    ~BoardModel();
signals:
    void draw();
    void whiteWon();
    void blackWon();
    void nextTurn();
    //For visual debugging and testing purposes
    void legalMovesCountChanged();
    void textChanged(QString text);
private:
    void notifyChanged(std::set<int> const& changed);
    void setStatus();
    QHash<int, QByteArray> _roleNames;
    int selectedIndex = -1;
    Board board;
    u64 legalMoves;
    PieceColor turn;
    BoardRecorder recorder;
};

#endif // BOARDMODEL_H
