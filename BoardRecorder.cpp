#include "BoardRecorder.h"

BoardRecorder::BoardRecorder()
{

}

void BoardRecorder::record(const Board &board)
{
    history.push_back(board);
    redoStack.clear();
}

Board BoardRecorder::undo()
{
    if (history.size() != 1) {
        auto back = history.back();
        history.pop_back();
        redoStack.push_back(back);
    }
    return history.back();
}

Board BoardRecorder::redo()
{
    if (redoStack.size() != 0) {
        auto back = redoStack.back();
        redoStack.pop_back();
        history.push_back(back);
    }
    return history.back();
}
