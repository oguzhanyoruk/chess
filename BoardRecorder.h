#ifndef BOARDRECORDER_H
#define BOARDRECORDER_H

#include "Board.h"

class BoardRecorder
{
public:
    BoardRecorder();
    void record(Board const& board);
    Board undo();
    Board redo();
private:
    std::vector<Board> history;
    std::vector<Board> redoStack;

};

#endif // BOARDRECORDER_H
