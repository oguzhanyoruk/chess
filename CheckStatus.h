#ifndef CHECKSTATUS_H
#define CHECKSTATUS_H

#include "BitIterator.h"

struct CheckStatus {
    bool isChecked = false;
    bool isDoubleChecked = false;
    u64 checkingPieces = 0x0;
    CheckStatus& addCheckingPiece(u64 const& piece) {
        if (checkingPieces) {
            isDoubleChecked = true;
        } else {
            isChecked = true;
        }
        checkingPieces |= piece;
        return *this;
    }
};

#endif // CHECKSTATUS_H
