QT += quick

linux-g++ | linux-g++-64 | linux-g++-32 {
 CONFIG += c++1z
 QMAKE_CXXFLAGS_RELEASE += -O3
}

win32 {
 CONFIG += c++17
 QMAKE_CXXFLAGS_RELEASE += /Ox
}

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        BitManip.cpp \
        BitIterator.cpp \
        Board.cpp \
        BoardModel.cpp \
        BoardRecorder.cpp \
        MoveLookupTable.cpp \
        Player.cpp \
        Settings.cpp \
        Square.cpp \
        main.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    BitManip.h \
    Board.h \
    BoardModel.h \
    BoardRecorder.h \
    CheckStatus.h \
    Common.h \
    JThread.h \
    Masks.h \
    Move.h \
    MoveLookupTable.h \
    BitIterator.h \
    Player.h \
    Settings.h \
    Square.h
