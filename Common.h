#ifndef COMMON_H
#define COMMON_H

#include <cstdint>

using u64 = std::uint64_t;
using u8 = std::uint8_t;

using PieceColor = bool;

static constexpr PieceColor White = true;
static constexpr PieceColor Black = false;

using PieceType = char;

static constexpr PieceType Pawn = ' ';
static constexpr PieceType Rook = 'R';
static constexpr PieceType Knight = 'N';
static constexpr PieceType Bishop = 'B';
static constexpr PieceType Queen = 'Q';
static constexpr PieceType King = 'K';

using SpecialNotation = char;

static constexpr SpecialNotation check = '+';
static constexpr SpecialNotation checkmate = '#';

#endif // COMMON_H
