import QtQuick 2.0

Rectangle {
    id:root
    Text {
        id:info
        anchors.horizontalCenter: parent.horizontalCenter
        text: "Legal Moves :" + boardModel.legalMovesCount
    }
    Connections {
        target: boardModel
        function onTextChanged(text) {
            info.text = "Legal Moves :" + boardModel.legalMovesCount + "\n" + text
        }
    }
}
