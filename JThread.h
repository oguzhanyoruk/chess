#ifndef JTHREAD_H
#define JTHREAD_H

#include <thread>

class JThread
{
public:
    template <class Callable, typename ...Args>
    explicit JThread(Callable&& c, Args&&... args)
    {
        thread = std::thread(c, std::forward<Args>(args)...);
    }
    ~JThread()
    {
        if (thread.joinable())
            thread.join();
    }
    JThread() = delete;
    JThread(JThread const&) = delete;
    JThread(JThread&& rhs)
    {
        thread = std::move(rhs.thread);
    }
    void operator=(JThread const&) = delete;
    JThread& operator=(JThread&& rhs)
    {
        thread = std::move(rhs.thread);
        return *this;
    }
private:
    std::thread thread;

};

#endif // JTHREAD_H
