#ifndef MASKS_H
#define MASKS_H

#include "Common.h"

#include <initializer_list>

namespace mask {
namespace board {
static constexpr u64 full  = 0xFFFFFFFFFFFFFFFF;
static constexpr u64 white = 0xAA55AA55AA55AA55;
static constexpr u64 black = 0x55AA55AA55AA55AA;
static constexpr u64 clear = 0x0000000000000000;
} // namespace board
namespace special {
namespace doublePawn {
static constexpr u8 a = 0x80;
static constexpr u8 b = 0x40;
static constexpr u8 c = 0x20;
static constexpr u8 d = 0x10;
static constexpr u8 e = 0x08;
static constexpr u8 f = 0x04;
static constexpr u8 g = 0x02;
static constexpr u8 h = 0x01;
static constexpr u64 multiplier = 0x0101010101010101;
static constexpr auto all = {a, b, c, d, e, f, g, h};
} // namespace doublePawn
namespace kingMoves {
static constexpr u8 white = 0x10;
static constexpr u8 black = 0x01;
//static constexpr u64 check = white | black;
}  // namespace kingMoves
namespace rookMoves {
namespace shortSide {
static constexpr u8 white = 0x20;
static constexpr u8 black = 0x02;
} // namespace shortSide
namespace longSide {
static constexpr u8 white = 0x40;
static constexpr u8 black = 0x04;
} // namespace longSide
} // namespace rookMoves
namespace castle {
namespace white {
static constexpr u8 checkShort = kingMoves::white | rookMoves::shortSide::white;
static constexpr u8 checkLong =  kingMoves::white | rookMoves::longSide::white;
} // namespace white
namespace black {
static constexpr u8 checkShort = kingMoves::black | rookMoves::shortSide::black;
static constexpr u8 checkLong =  kingMoves::black | rookMoves::longSide::black;
} // namespace black
} // namespace castle
} // namespace special

namespace file {
static constexpr u64 a = 0x8080808080808080;
static constexpr u64 b = 0x4040404040404040;
static constexpr u64 c = 0x2020202020202020;
static constexpr u64 d = 0x1010101010101010;
static constexpr u64 e = 0x0808080808080808;
static constexpr u64 f = 0x0404040404040404;
static constexpr u64 g = 0x0202020202020202;
static constexpr u64 h = 0x0101010101010101;
static constexpr auto all = {a, b, c, d, e, f, g, h};
} // namespace file

namespace rank {
static constexpr u64 _1 = 0x00000000000000FF;
static constexpr u64 _2 = 0x000000000000FF00;
static constexpr u64 _3 = 0x0000000000FF0000;
static constexpr u64 _4 = 0x00000000FF000000;
static constexpr u64 _5 = 0x000000FF00000000;
static constexpr u64 _6 = 0x0000FF0000000000;
static constexpr u64 _7 = 0x00FF000000000000;
static constexpr u64 _8 = 0xFF00000000000000;
static constexpr auto all = {_1, _2, _3, _4, _5, _6, _7 ,_8};
} // namespace rank

namespace square {
static constexpr u64 a1 = file::a & rank::_1;
static constexpr u64 a2 = file::a & rank::_2;
static constexpr u64 a3 = file::a & rank::_3;
static constexpr u64 a4 = file::a & rank::_4;
static constexpr u64 a5 = file::a & rank::_5;
static constexpr u64 a6 = file::a & rank::_6;
static constexpr u64 a7 = file::a & rank::_7;
static constexpr u64 a8 = file::a & rank::_8;
static constexpr u64 b1 = file::b & rank::_1;
static constexpr u64 b2 = file::b & rank::_2;
static constexpr u64 b3 = file::b & rank::_3;
static constexpr u64 b4 = file::b & rank::_4;
static constexpr u64 b5 = file::b & rank::_5;
static constexpr u64 b6 = file::b & rank::_6;
static constexpr u64 b7 = file::b & rank::_7;
static constexpr u64 b8 = file::b & rank::_8;
static constexpr u64 c1 = file::c & rank::_1;
static constexpr u64 c2 = file::c & rank::_2;
static constexpr u64 c3 = file::c & rank::_3;
static constexpr u64 c4 = file::c & rank::_4;
static constexpr u64 c5 = file::c & rank::_5;
static constexpr u64 c6 = file::c & rank::_6;
static constexpr u64 c7 = file::c & rank::_7;
static constexpr u64 c8 = file::c & rank::_8;
static constexpr u64 d1 = file::d & rank::_1;
static constexpr u64 d2 = file::d & rank::_2;
static constexpr u64 d3 = file::d & rank::_3;
static constexpr u64 d4 = file::d & rank::_4;
static constexpr u64 d5 = file::d & rank::_5;
static constexpr u64 d6 = file::d & rank::_6;
static constexpr u64 d7 = file::d & rank::_7;
static constexpr u64 d8 = file::d & rank::_8;
static constexpr u64 e1 = file::e & rank::_1;
static constexpr u64 e2 = file::e & rank::_2;
static constexpr u64 e3 = file::e & rank::_3;
static constexpr u64 e4 = file::e & rank::_4;
static constexpr u64 e5 = file::e & rank::_5;
static constexpr u64 e6 = file::e & rank::_6;
static constexpr u64 e7 = file::e & rank::_7;
static constexpr u64 e8 = file::e & rank::_8;
static constexpr u64 f1 = file::f & rank::_1;
static constexpr u64 f2 = file::f & rank::_2;
static constexpr u64 f3 = file::f & rank::_3;
static constexpr u64 f4 = file::f & rank::_4;
static constexpr u64 f5 = file::f & rank::_5;
static constexpr u64 f6 = file::f & rank::_6;
static constexpr u64 f7 = file::f & rank::_7;
static constexpr u64 f8 = file::f & rank::_8;
static constexpr u64 g1 = file::g & rank::_1;
static constexpr u64 g2 = file::g & rank::_2;
static constexpr u64 g3 = file::g & rank::_3;
static constexpr u64 g4 = file::g & rank::_4;
static constexpr u64 g5 = file::g & rank::_5;
static constexpr u64 g6 = file::g & rank::_6;
static constexpr u64 g7 = file::g & rank::_7;
static constexpr u64 g8 = file::g & rank::_8;
static constexpr u64 h1 = file::h & rank::_1;
static constexpr u64 h2 = file::h & rank::_2;
static constexpr u64 h3 = file::h & rank::_3;
static constexpr u64 h4 = file::h & rank::_4;
static constexpr u64 h5 = file::h & rank::_5;
static constexpr u64 h6 = file::h & rank::_6;
static constexpr u64 h7 = file::h & rank::_7;
static constexpr u64 h8 = file::h & rank::_8;
static constexpr auto all = {
    a1,a2,a3,a4,a5,a6,a7,a8,
    b1,b2,b3,b4,b5,b6,b7,b8,
    c1,c2,c3,c4,c5,c6,c7,c8,
    d1,d2,d3,d4,d5,d6,d7,d8,
    e1,e2,e3,e4,e5,e6,e7,e8,
    f1,f2,f3,f4,f5,f6,f7,f8,
    g1,g2,g3,g4,g5,g6,g7,g8,
    h1,h2,h3,h4,h5,h6,h7,h8
};
} // namespace square

namespace standard {
namespace white {
namespace castle {
static constexpr u64 shortSideInBetween = square::f1 | square::g1;
static constexpr u64 longSideInBetween  = square::b1 | square::c1 | square::d1;
} // namespace castle
} // namespace white
namespace black {
namespace castle {
static constexpr u64 shortSideInBetween = square::f8 | square::g8;
static constexpr u64 longSideInBetween  = square::b8 | square::c8 | square::d8;
} // namespace castle
} // namespace black
} // namespace standard

namespace diagonal {
namespace white {
namespace left {
static constexpr u64 a8toa8 = square::a8;
static constexpr u64 a6toc8 = square::a6 | square::b7 | square::c8;
static constexpr u64 a4toe8 = square::a4 | square::b5 | square::c6 | square::d7 | square::e8;
static constexpr u64 a2tog8 = square::a2 | square::b3 | square::c4 | square::d5 | square::e6 | square::f7 | square::g8;
static constexpr u64 b1toh7 = square::b1 | square::c2 | square::d3 | square::e4 | square::f5 | square::g6 | square::h7;
static constexpr u64 d1toh5 = square::d1 | square::e2 | square::f3 | square::g4 | square::h5;
static constexpr u64 f1toh3 = square::f1 | square::g2 | square::h3;
static constexpr u64 h1toh1 = square::h1;
static constexpr auto all = {a8toa8, a6toc8, a4toe8, a2tog8, b1toh7, d1toh5, f1toh3, h1toh1};
} // namespace left
namespace right {
static constexpr u64 h7tog8 = square::h7 | square::g8;
static constexpr u64 h5toe8 = square::h5 | square::g6 | square::f7 | square::e8;
static constexpr u64 h3toc8 = square::h3 | square::g4 | square::f5 | square::e6 | square::d7 | square::c8;
static constexpr u64 h1toa8 = square::h1 | square::g2 | square::f3 | square::e4 | square::d5 | square::c6 | square::b7 | square::a8;
static constexpr u64 f1toa6 = square::f1 | square::e2 | square::d3 | square::c4 | square::b5 | square::a6;
static constexpr u64 d1toa4 = square::d1 | square::c2 | square::b3 | square::a4;
static constexpr u64 b1toa2 = square::b1 | square::a2;
static constexpr auto all = {h7tog8, h5toe8, h3toc8, h1toa8, f1toa6, d1toa4, b1toa2};
} // namespace right
static constexpr auto all = {
    left::a8toa8,  left::a6toc8,  left::a4toe8,  left::a2tog8,  left::b1toh7,  left::d1toh5,  left::f1toh3, left::h1toh1,
    right::h7tog8, right::h5toe8, right::h3toc8, right::h1toa8, right::f1toa6, right::d1toa4, right::b1toa2
};
} // namespace white
namespace black {
namespace left {
static constexpr u64 a7tob8 = square::a7 | square::b8;
static constexpr u64 a5tod8 = square::a5 | square::b6 | square::c7 | square::d8;
static constexpr u64 a3tof8 = square::a3 | square::b4 | square::c5 | square::d6 | square::e7 | square::f8;
static constexpr u64 a1toh8 = square::a1 | square::b2 | square::c3 | square::d4 | square::e5 | square::f6 | square::g7 | square::h8;
static constexpr u64 c1toh6 = square::c1 | square::d2 | square::e3 | square::f4 | square::g5 | square::h6;
static constexpr u64 e1toh4 = square::e1 | square::f2 | square::g3 | square::h4;
static constexpr u64 g1toh2 = square::g1 | square::h2;
static constexpr auto all = {a7tob8, a5tod8, a3tof8, a1toh8, c1toh6, e1toh4, g1toh2};
} // namespace left
namespace right {
static constexpr u64 h8toh8 = square::h8;
static constexpr u64 h6tof8 = square::h6 | square::g7 | square::f8;
static constexpr u64 h4tod8 = square::h4 | square::g5 | square::f6 | square::e7 | square::d8;
static constexpr u64 h2tob8 = square::h2 | square::g3 | square::f4 | square::e5 | square::d6 | square::c7 | square::b8;
static constexpr u64 g1toa7 = square::g1 | square::f2 | square::e3 | square::d4 | square::c5 | square::b6 | square::a7;
static constexpr u64 e1toa5 = square::e1 | square::d2 | square::c3 | square::b4 | square::a5;
static constexpr u64 c1toa3 = square::c1 | square::b2 | square::a3;
static constexpr u64 a1toa1 = square::a1;
static constexpr auto all = {h8toh8, h6tof8, h4tod8, h2tob8, g1toa7, e1toa5, c1toa3, a1toa1};
} // namespace right
static constexpr auto all = {
    left::a7tob8,  left::a5tod8,  left::a3tof8,  left::a1toh8,  left::c1toh6,  left::e1toh4,  left::g1toh2,
    right::h8toh8, right::h6tof8, right::h4tod8, right::h2tob8, right::g1toa7, right::e1toa5, right::c1toa3, right::a1toa1
};
} // namespace black
} // namespace diagonal
}// namespace mask
#endif // MASKS_H
