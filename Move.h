#ifndef MOVE_H
#define MOVE_H

#include "Common.h"

struct Move {
    Move(u64 const& from, u64 const& to)
        : from(from),
          to(to)
    {

    }
    u64 from;
    u64 to;
};

#endif // MOVE_H
