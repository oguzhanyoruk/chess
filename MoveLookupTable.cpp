
#include "MoveLookupTable.h"

#include <algorithm>
#include <QDebug>

using namespace mask::square;
static constexpr u64 toIndex[64] = {
    a1,b1,c1,d1,e1,f1,g1,h1,
    a2,b2,c2,d2,e2,f2,g2,h2,
    a3,b3,c3,d3,e3,f3,g3,h3,
    a4,b4,c4,d4,e4,f4,g4,h4,
    a5,b5,c5,d5,e5,f5,g5,h5,
    a6,b6,c6,d6,e6,f6,g6,h6,
    a7,b7,c7,d7,e7,f7,g7,h7,
    a8,b8,c8,d8,e8,f8,g8,h8

};
int mlt::index(u64 const& square) {
    auto i = std::find(std::begin(toIndex), std::end(toIndex), square) - std::begin(toIndex);
    auto ri = (63 - i) / 8;
    auto fi = 7 - (63 - i) % 8;
    return ri * 8 + fi;
}

namespace {

int index(u64 const& square) {
    return std::find(std::begin(toIndex), std::end(toIndex), square) - std::begin(toIndex);
}


static constexpr u64 whitePawnMoveTable[64] = {
    a2, b2, c2, d2, e2, f2, g2, h2,
    a3 | a4, b3 | b4, c3 | c4, d3 | d4, e3 | e4, f3 | f4, g3 | g4, h3 | h4,
    a4, b4, c4, d4, e4, f4, g4, h4,
    a5, b5, c5, d5, e5, f5, g5, h5,
    a6, b6, c6, d6, e6, f6, g6, h6,
    a7, b7, c7, d7, e7, f7, g7, h7,
    a8, b8, c8, d8, e8, f8, g8, h8,
    0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0
};

static constexpr u64 blackPawnMoveTable[64] = {
    0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
    a1, b1, c1, d1, e1, f1, g1, h1,
    a2, b2, c2, d2, e2, f2, g2, h2,
    a3, b3, c3, d3, e3, f3, g3, h3,
    a4, b4, c4, d4, e4, f4, g4, h4,
    a5, b5, c5, d5, e5, f5, g5, h5,
    a6 | a5, b6 | b5, c6 | c5, d6 | d5, e6 | e5, f6 | f5, g6 | g5, h6 | h5,
    a7, b7, c7, d7, e7, f7, g7, h7
};

static constexpr u64 whitePawnAttackTable[64] = {
    b2, a2 | c2, b2 | d2, c2 | e2, d2 | f2, e2 | g2, f2 | h2, g2,
    b3, a3 | c3, b3 | d3, c3 | e3, d3 | f3, e3 | g3, f3 | h3, g3,
    b4, a4 | c4, b4 | d4, c4 | e4, d4 | f4, e4 | g4, f4 | h4, g4,
    b5, a5 | c5, b5 | d5, c5 | e5, d5 | f5, e5 | g5, f5 | h5, g5,
    b6, a6 | c6, b6 | d6, c6 | e6, d6 | f6, e6 | g6, f6 | h6, g6,
    b7, a7 | c7, b7 | d7, c7 | e7, d7 | f7, e7 | g7, f7 | h7, g7,
    b8, a8 | c8, b8 | d8, c8 | e8, d8 | f8, e8 | g8, f8 | h8, g8,
    0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
};

static constexpr u64 blackPawnAttackTable[64] = {
    0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
    b1, a1 | c1, b1 | d1, c1 | e1, d1 | f1, e1 | g1, f1 | h1, g1,
    b2, a2 | c2, b2 | d2, c2 | e2, d2 | f2, e2 | g2, f2 | h2, g2,
    b3, a3 | c3, b3 | d3, c3 | e3, d3 | f3, e3 | g3, f3 | h3, g3,
    b4, a4 | c4, b4 | d4, c4 | e4, d4 | f4, e4 | g4, f4 | h4, g4,
    b5, a5 | c5, b5 | d5, c5 | e5, d5 | f5, e5 | g5, f5 | h5, g5,
    b6, a6 | c6, b6 | d6, c6 | e6, d6 | f6, e6 | g6, f6 | h6, g6,
    b7, a7 | c7, b7 | d7, c7 | e7, d7 | f7, e7 | g7, f7 | h7, g7,
};



static constexpr u64 rookSouthTable[64] = {
    0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
    a1, b1, c1, d1, e1, f1, g1, h1,
	a1 | a2,
	b1 | b2,
	c1 | c2,
	d1 | d2,
	e1 | e2,
	f1 | f2,
	g1 | g2,
	h1 | h2,
	a1 | a2 | a3,
	b1 | b2 | b3,
	c1 | c2 | c3,
	d1 | d2 | d3,
	e1 | e2 | e3,
	f1 | f2 | f3,
	g1 | g2 | g3,
	h1 | h2 | h3,
	a1 | a2 | a3 | a4,
	b1 | b2 | b3 | b4,
	c1 | c2 | c3 | c4,
	d1 | d2 | d3 | d4,
	e1 | e2 | e3 | e4,
	f1 | f2 | f3 | f4,
	g1 | g2 | g3 | g4,
	h1 | h2 | h3 | h4,
	a1 | a2 | a3 | a4 | a5,
	b1 | b2 | b3 | b4 | b5,
	c1 | c2 | c3 | c4 | c5,
	d1 | d2 | d3 | d4 | d5,
	e1 | e2 | e3 | e4 | e5,
	f1 | f2 | f3 | f4 | f5,
	g1 | g2 | g3 | g4 | g5,
	h1 | h2 | h3 | h4 | h5,
	a1 | a2 | a3 | a4 | a5 | a6,
	b1 | b2 | b3 | b4 | b5 | b6,
	c1 | c2 | c3 | c4 | c5 | c6,
	d1 | d2 | d3 | d4 | d5 | d6,
	e1 | e2 | e3 | e4 | e5 | e6,
	f1 | f2 | f3 | f4 | f5 | f6,
	g1 | g2 | g3 | g4 | g5 | g6,
	h1 | h2 | h3 | h4 | h5 | h6,
	a1 | a2 | a3 | a4 | a5 | a6 | a7,
	b1 | b2 | b3 | b4 | b5 | b6 | b7,
	c1 | c2 | c3 | c4 | c5 | c6 | c7,
	d1 | d2 | d3 | d4 | d5 | d6 | d7,
	e1 | e2 | e3 | e4 | e5 | e6 | e7,
	f1 | f2 | f3 | f4 | f5 | f6 | f7,
	g1 | g2 | g3 | g4 | g5 | g6 | g7,
	h1 | h2 | h3 | h4 | h5 | h6 | h7
};

static constexpr u64 rookEastTable[64] = {
	b1 | c1 | d1 | e1 | f1 | g1 | h1,
	c1 | d1 | e1 | f1 | g1 | h1,
	d1 | e1 | f1 | g1 | h1,
	e1 | f1 | g1 | h1,
	f1 | g1 | h1,
	g1 | h1,
	h1,
	0x0,
	b2 | c2 | d2 | e2 | f2 | g2 | h2,
	c2 | d2 | e2 | f2 | g2 | h2,
	d2 | e2 | f2 | g2 | h2,
	e2 | f2 | g2 | h2,
	f2 | g2 | h2,
	g2 | h2,
	h2,
	0x0,
	b3 | c3 | d3 | e3 | f3 | g3 | h3,
	c3 | d3 | e3 | f3 | g3 | h3,
	d3 | e3 | f3 | g3 | h3,
	e3 | f3 | g3 | h3,
	f3 | g3 | h3,
	g3 | h3,
	h3,
	0x0,
	b4 | c4 | d4 | e4 | f4 | g4 | h4,
	c4 | d4 | e4 | f4 | g4 | h4,
	d4 | e4 | f4 | g4 | h4,
	e4 | f4 | g4 | h4,
	f4 | g4 | h4,
	g4 | h4,
	h4,
	0x0,
	b5 | c5 | d5 | e5 | f5 | g5 | h5,
	c5 | d5 | e5 | f5 | g5 | h5,
	d5 | e5 | f5 | g5 | h5,
	e5 | f5 | g5 | h5,
	f5 | g5 | h5,
	g5 | h5,
	h5,
	0x0,
	b6 | c6 | d6 | e6 | f6 | g6 | h6,
	c6 | d6 | e6 | f6 | g6 | h6,
	d6 | e6 | f6 | g6 | h6,
	e6 | f6 | g6 | h6,
	f6 | g6 | h6,
	g6 | h6,
	h6,
	0x0,
	b7 | c7 | d7 | e7 | f7 | g7 | h7,
	c7 | d7 | e7 | f7 | g7 | h7,
	d7 | e7 | f7 | g7 | h7,
	e7 | f7 | g7 | h7,
	f7 | g7 | h7,
	g7 | h7,
	h7,
	0x0,
	b8 | c8 | d8 | e8 | f8 | g8 | h8,
	c8 | d8 | e8 | f8 | g8 | h8,
	d8 | e8 | f8 | g8 | h8,
	e8 | f8 | g8 | h8,
	f8 | g8 | h8,
	g8 | h8,
	h8,
	0x0 
};

static constexpr u64 rookNorthTable[64] = {
	a2 | a3 | a4 | a5 | a6 | a7 | a8,
	b2 | b3 | b4 | b5 | b6 | b7 | b8,
	c2 | c3 | c4 | c5 | c6 | c7 | c8,
	d2 | d3 | d4 | d5 | d6 | d7 | d8,
	e2 | e3 | e4 | e5 | e6 | e7 | e8,
	f2 | f3 | f4 | f5 | f6 | f7 | f8,
	g2 | g3 | g4 | g5 | g6 | g7 | g8,
	h2 | h3 | h4 | h5 | h6 | h7 | h8,
	a3 | a4 | a5 | a6 | a7 | a8,
	b3 | b4 | b5 | b6 | b7 | b8,
	c3 | c4 | c5 | c6 | c7 | c8,
	d3 | d4 | d5 | d6 | d7 | d8,
	e3 | e4 | e5 | e6 | e7 | e8,
	f3 | f4 | f5 | f6 | f7 | f8,
	g3 | g4 | g5 | g6 | g7 | g8,
	h3 | h4 | h5 | h6 | h7 | h8,
	a4 | a5 | a6 | a7 | a8,
	b4 | b5 | b6 | b7 | b8,
	c4 | c5 | c6 | c7 | c8,
	d4 | d5 | d6 | d7 | d8,
	e4 | e5 | e6 | e7 | e8,
	f4 | f5 | f6 | f7 | f8,
	g4 | g5 | g6 | g7 | g8,
	h4 | h5 | h6 | h7 | h8,
	a5 | a6 | a7 | a8,
	b5 | b6 | b7 | b8,
	c5 | c6 | c7 | c8,
	d5 | d6 | d7 | d8,
	e5 | e6 | e7 | e8,
	f5 | f6 | f7 | f8,
	g5 | g6 | g7 | g8,
	h5 | h6 | h7 | h8,
	a6 | a7 | a8,
	b6 | b7 | b8,
	c6 | c7 | c8,
	d6 | d7 | d8,
	e6 | e7 | e8,
	f6 | f7 | f8,
	g6 | g7 | g8,
	h6 | h7 | h8,
	a7 | a8,
	b7 | b8,
	c7 | c8,
	d7 | d8,
	e7 | e8,
	f7 | f8,
	g7 | g8,
	h7 | h8,
	a8, b8, c8, d8, e8, f8, g8, h8,
	0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0
};

static constexpr u64 rookWestTable[64] = {
	0x0,
	a1,
	a1 | b1,
	a1 | b1 | c1,
	a1 | b1 | c1 | d1,
	a1 | b1 | c1 | d1 | e1,
	a1 | b1 | c1 | d1 | e1 | f1,
	a1 | b1 | c1 | d1 | e1 | f1 | g1,
	0x0,
	a2,
	a2 | b2,
	a2 | b2 | c2,
	a2 | b2 | c2 | d2,
	a2 | b2 | c2 | d2 | e2,
	a2 | b2 | c2 | d2 | e2 | f2,
	a2 | b2 | c2 | d2 | e2 | f2 | g2,
	0x0,
	a3,
	a3 | b3,
	a3 | b3 | c3,
	a3 | b3 | c3 | d3,
	a3 | b3 | c3 | d3 | e3,
	a3 | b3 | c3 | d3 | e3 | f3,
	a3 | b3 | c3 | d3 | e3 | f3 | g3,
	0x0,
	a4,
	a4 | b4,
	a4 | b4 | c4,
	a4 | b4 | c4 | d4,
	a4 | b4 | c4 | d4 | e4,
	a4 | b4 | c4 | d4 | e4 | f4,
	a4 | b4 | c4 | d4 | e4 | f4 | g4,
	0x0,
	a5,
	a5 | b5,
	a5 | b5 | c5,
	a5 | b5 | c5 | d5,
	a5 | b5 | c5 | d5 | e5,
	a5 | b5 | c5 | d5 | e5 | f5,
	a5 | b5 | c5 | d5 | e5 | f5 | g5,
	0x0,
	a6,
	a6 | b6,
	a6 | b6 | c6,
	a6 | b6 | c6 | d6,
	a6 | b6 | c6 | d6 | e6,
	a6 | b6 | c6 | d6 | e6 | f6,
	a6 | b6 | c6 | d6 | e6 | f6 | g6,
	0x0,
	a7,
	a7 | b7,
	a7 | b7 | c7,
	a7 | b7 | c7 | d7,
	a7 | b7 | c7 | d7 | e7,
	a7 | b7 | c7 | d7 | e7 | f7,
	a7 | b7 | c7 | d7 | e7 | f7 | g7,
	0x0,
	a8,
	a8 | b8,
	a8 | b8 | c8,
	a8 | b8 | c8 | d8,
	a8 | b8 | c8 | d8 | e8,
	a8 | b8 | c8 | d8 | e8 | f8,
	a8 | b8 | c8 | d8 | e8 | f8 | g8 
};

static constexpr u64 knightSoutheastTable[64] = {
	0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
	c1, d1, e1, f1, g1, h1, 0x0, 0x0,
	c2 | b1,
	d2 | c1,
	e2 | d1,
	f2 | e1,
	g2 | f1,
	h2 | g1,
	h1,
	0x0,
	c3 | b2,
	d3 | c2,
	e3 | d2,
	f3 | e2,
	g3 | f2,
	h3 | g2,
	h2,
	0x0,
	c4 | b3,
	d4 | c3,
	e4 | d3,
	f4 | e3,
	g4 | f3,
	h4 | g3,
	h3,
	0x0,
	c5 | b4,
	d5 | c4,
	e5 | d4,
	f5 | e4,
	g5 | f4,
	h5 | g4,
	h4,
	0x0,
	c6 | b5,
	d6 | c5,
	e6 | d5,
	f6 | e5,
	g6 | f5,
	h6 | g5,
	h5,
	0x0,
	c7 | b6,
	d7 | c6,
	e7 | d6,
	f7 | e6,
	g7 | f6,
	h7 | g6,
	h6,
	0x0 
};

static constexpr u64 knightNortheastTable[64] = {
	c2 | b3,
	d2 | c3,
	e2 | d3,
	f2 | e3,
	g2 | f3,
	h2 | g3,
	h3,
	0x0,
	c3 | b4,
	d3 | c4,
	e3 | d4,
	f3 | e4,
	g3 | f4,
	h3 | g4,
	h4,
	0x0,
	c4 | b5,
	d4 | c5,
	e4 | d5,
	f4 | e5,
	g4 | f5,
	h4 | g5,
	h5,
	0x0,
	c5 | b6,
	d5 | c6,
	e5 | d6,
	f5 | e6,
	g5 | f6,
	h5 | g6,
	h6,
	0x0,
	c6 | b7,
	d6 | c7,
	e6 | d7,
	f6 | e7,
	g6 | f7,
	h6 | g7,
	h7,
	0x0,
	c7 | b8,
	d7 | c8,
	e7 | d8,
	f7 | e8,
	g7 | f8,
	h7 | g8,
	h8, 0x0, c8, d8, e8, f8, g8, h8,
	0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
	0x0,
	0x0 
};

static constexpr u64 knightNorthwestTable[64] = {
	0x0,
	a3,
	a2 | b3,
	b2 | c3,
	c2 | d3,
	d2 | e3,
	e2 | f3,
	f2 | g3,
	0x0,
	a4,
	a3 | b4,
	b3 | c4,
	c3 | d4,
	d3 | e4,
	e3 | f4,
	f3 | g4,
	0x0,
	a5,
	a4 | b5,
	b4 | c5,
	c4 | d5,
	d4 | e5,
	e4 | f5,
	f4 | g5,
	0x0,
	a6,
	a5 | b6,
	b5 | c6,
	c5 | d6,
	d5 | e6,
	e5 | f6,
	f5 | g6,
	0x0,
	a7,
	a6 | b7,
	b6 | c7,
	c6 | d7,
	d6 | e7,
	e6 | f7,
	f6 | g7,
	0x0,
	a8,
	a7 | b8,
	b7 | c8,
	c7 | d8,
	d7 | e8,
	e7 | f8,
	f7 | g8,
	0x0, 0x0, a8, b8, c8, d8, e8, f8,
	0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
};

static constexpr u64 knightSouthwestTable[64] = {
	0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
	0x0, 0x0, a1, b1, c1, d1, e1, f1,
	0x0,
	a1,
	a2 | b1,
	b2 | c1,
	c2 | d1,
	d2 | e1,
	e2 | f1,
	f2 | g1,
	0x0,
	a2,
	a3 | b2,
	b3 | c2,
	c3 | d2,
	d3 | e2,
	e3 | f2,
	f3 | g2,
	0x0,
	a3,
	a4 | b3,
	b4 | c3,
	c4 | d3,
	d4 | e3,
	e4 | f3,
	f4 | g3,
	0x0,
	a4,
	a5 | b4,
	b5 | c4,
	c5 | d4,
	d5 | e4,
	e5 | f4,
	f5 | g4,
	0x0,
	a5,
	a6 | b5,
	b6 | c5,
	c6 | d5,
	d6 | e5,
	e6 | f5,
	f6 | g5,
	0x0,
	a6,
	a7 | b6,
	b7 | c6,
	c7 | d6,
	d7 | e6,
	e7 | f6,
	f7 | g6 
};

static constexpr u64 bishopSoutheastTable[64] = {
	0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
	b1, c1, d1, e1, f1, g1, h1, 0x0,
	b2 | c1,
	c2 | d1,
	d2 | e1,
	e2 | f1,
	f2 | g1,
	g2 | h1,
	h2,
	0x0,
	b3 | c2 | d1,
	c3 | d2 | e1,
	d3 | e2 | f1,
	e3 | f2 | g1,
	f3 | g2 | h1,
	g3 | h2,
	h3,
	0x0,
	b4 | c3 | d2 | e1,
	c4 | d3 | e2 | f1,
	d4 | e3 | f2 | g1,
	e4 | f3 | g2 | h1,
	f4 | g3 | h2,
	g4 | h3,
	h4,
	0x0,
	b5 | c4 | d3 | e2 | f1,
	c5 | d4 | e3 | f2 | g1,
	d5 | e4 | f3 | g2 | h1,
	e5 | f4 | g3 | h2,
	f5 | g4 | h3,
	g5 | h4,
	h5,
	0x0,
	b6 | c5 | d4 | e3 | f2 | g1,
	c6 | d5 | e4 | f3 | g2 | h1,
	d6 | e5 | f4 | g3 | h2,
	e6 | f5 | g4 | h3,
	f6 | g5 | h4,
	g6 | h5,
	h6,
	0x0,
	b7 | c6 | d5 | e4 | f3 | g2 | h1,
	c7 | d6 | e5 | f4 | g3 | h2,
	d7 | e6 | f5 | g4 | h3,
	e7 | f6 | g5 | h4,
	f7 | g6 | h5,
	g7 | h6,
	h7,
	0x0 
};

static constexpr u64 bishopNortheastTable[64] = {
	b2 | c3 | d4 | e5 | f6 | g7 | h8,
	c2 | d3 | e4 | f5 | g6 | h7,
	d2 | e3 | f4 | g5 | h6,
	e2 | f3 | g4 | h5,
	f2 | g3 | h4,
	g2 | h3,
	h2,
	0x0,
	b3 | c4 | d5 | e6 | f7 | g8,
	c3 | d4 | e5 | f6 | g7 | h8,
	d3 | e4 | f5 | g6 | h7,
	e3 | f4 | g5 | h6,
	f3 | g4 | h5,
	g3 | h4,
	h3,
	0x0,
	b4 | c5 | d6 | e7 | f8,
	c4 | d5 | e6 | f7 | g8,
	d4 | e5 | f6 | g7 | h8,
	e4 | f5 | g6 | h7,
	f4 | g5 | h6,
	g4 | h5,
	h4,
	0x0,
	b5 | c6 | d7 | e8,
	c5 | d6 | e7 | f8,
	d5 | e6 | f7 | g8,
	e5 | f6 | g7 | h8,
	f5 | g6 | h7,
	g5 | h6,
	h5,
	0x0,
	b6 | c7 | d8,
	c6 | d7 | e8,
	d6 | e7 | f8,
	e6 | f7 | g8,
	f6 | g7 | h8,
	g6 | h7,
	h6,
	0x0,
	b7 | c8,
	c7 | d8,
	d7 | e8,
	e7 | f8,
	f7 | g8,
	g7 | h8,
	h7, 
    0x0, b8, c8, d8, e8, f8, g8, h8, 0x0, 
    0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 
};

static constexpr u64 bishopNorthwestTable[64] = {
	0x0,
	a2,
	a3 | b2,
	a4 | b3 | c2,
	a5 | b4 | c3 | d2,
	a6 | b5 | c4 | d3 | e2,
	a7 | b6 | c5 | d4 | e3 | f2,
	a8 | b7 | c6 | d5 | e4 | f3 | g2,
	0x0,
	a3,
	a4 | b3,
	a5 | b4 | c3,
	a6 | b5 | c4 | d3,
	a7 | b6 | c5 | d4 | e3,
	a8 | b7 | c6 | d5 | e4 | f3,
	b8 | c7 | d6 | e5 | f4 | g3,
	0x0,
	a4,
	a5 | b4,
	a6 | b5 | c4,
	a7 | b6 | c5 | d4,
	a8 | b7 | c6 | d5 | e4,
	b8 | c7 | d6 | e5 | f4,
	c8 | d7 | e6 | f5 | g4,
	0x0,
	a5,
	a6 | b5,
	a7 | b6 | c5,
	a8 | b7 | c6 | d5,
	b8 | c7 | d6 | e5,
	c8 | d7 | e6 | f5,
	d8 | e7 | f6 | g5,
	0x0,
	a6,
	a7 | b6,
	a8 | b7 | c6,
	b8 | c7 | d6,
	c8 | d7 | e6,
	d8 | e7 | f6,
	e8 | f7 | g6,
	0x0,
	a7,
	a8 | b7,
	b8 | c7,
	c8 | d7,
	d8 | e7,
	e8 | f7,
	f8 | g7,
	0x0, a8, b8, c8, d8, e8, f8, g8,
	0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
};

static constexpr u64 bishopSouthwestTable[64] = {
	0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
	0x0, a1, b1, c1, d1, e1, f1, g1,
	0x0,
	a2,
	a1 | b2,
	b1 | c2,
	c1 | d2,
	d1 | e2,
	e1 | f2,
	f1 | g2,
	0x0,
	a3,
	a2 | b3,
	a1 | b2 | c3,
	b1 | c2 | d3,
	c1 | d2 | e3,
	d1 | e2 | f3,
	e1 | f2 | g3,
	0x0,
	a4,
	a3 | b4,
	a2 | b3 | c4,
	a1 | b2 | c3 | d4,
	b1 | c2 | d3 | e4,
	c1 | d2 | e3 | f4,
	d1 | e2 | f3 | g4,
	0x0,
	a5,
	a4 | b5,
	a3 | b4 | c5,
	a2 | b3 | c4 | d5,
	a1 | b2 | c3 | d4 | e5,
	b1 | c2 | d3 | e4 | f5,
	c1 | d2 | e3 | f4 | g5,
	0x0,
	a6,
	a5 | b6,
	a4 | b5 | c6,
	a3 | b4 | c5 | d6,
	a2 | b3 | c4 | d5 | e6,
	a1 | b2 | c3 | d4 | e5 | f6,
	b1 | c2 | d3 | e4 | f5 | g6,
	0x0,
	a7,
	a6 | b7,
	a5 | b6 | c7,
	a4 | b5 | c6 | d7,
	a3 | b4 | c5 | d6 | e7,
	a2 | b3 | c4 | d5 | e6 | f7,
	a1 | b2 | c3 | d4 | e5 | f6 | g7 
};

static constexpr u64 kingSouthTable[64] = {
	0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
	a1, b1, c1, d1, e1, f1, g1, h1,
	a2, b2, c2, d2, e2, f2, g2, h2,
	a3, b3, c3, d3, e3, f3, g3, h3,
	a4, b4, c4, d4, e4, f4, g4, h4,
	a5, b5, c5, d5, e5, f5, g5, h5,
	a6, b6, c6, d6, e6, f6, g6, h6,
    a7, b7, c7, d7, e7, f7, g7, h7,

};

static constexpr u64 kingSoutheastTable[64] = {
	0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
	b1, c1, d1, e1, f1, g1, h1, 0x0,
	b2, c2, d2, e2, f2, g2, h2, 0x0,
	b3, c3, d3, e3, f3, g3, h3, 0x0,
	b4, c4, d4, e4, f4, g4, h4, 0x0,
	b5, c5, d5, e5, f5, g5, h5, 0x0,
	b6, c6, d6, e6, f6, g6, h6, 0x0,
	b7, c7, d7, e7, f7, g7, h7, 0x0,
};

static constexpr u64 kingEastTable[64] = {
	b1, c1, d1, e1, f1, g1, h1, 0x0,
	b2, c2, d2, e2, f2, g2, h2, 0x0,
	b3, c3, d3, e3, f3, g3, h3, 0x0,
	b4, c4, d4, e4, f4, g4, h4, 0x0,
	b5, c5, d5, e5, f5, g5, h5, 0x0,
	b6, c6, d6, e6, f6, g6, h6, 0x0,
	b7, c7, d7, e7, f7, g7, h7, 0x0,
	b8, c8, d8, e8, f8, g8, h8, 0x0,
};

static constexpr u64 kingNortheastTable[64] = {
	b2, c2, d2, e2, f2, g2, h2, 0x0,
	b3, c3, d3, e3, f3, g3, h3, 0x0,
	b4, c4, d4, e4, f4, g4, h4, 0x0,
	b5, c5, d5, e5, f5, g5, h5, 0x0,
	b6, c6, d6, e6, f6, g6, h6, 0x0,
	b7, c7, d7, e7, f7, g7, h7, 0x0,
	b8, c8, d8, e8, f8, g8, h8, 0x0,
	0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
};

static constexpr u64 kingNorthTable[64] = {
	a2, b2, c2, d2, e2, f2, g2, h2,
	a3, b3, c3, d3, e3, f3, g3, h3,
	a4, b4, c4, d4, e4, f4, g4, h4,
	a5, b5, c5, d5, e5, f5, g5, h5,
	a6, b6, c6, d6, e6, f6, g6, h6,
	a7, b7, c7, d7, e7, f7, g7, h7,
	a8, b8, c8, d8, e8, f8, g8, h8,
	0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
};

static constexpr u64 kingNorthwestTable[64] = {
	0x0, a2, b2, c2, d2, e2, f2, g2,
	0x0, a3, b3, c3, d3, e3, f3, g3,
	0x0, a4, b4, c4, d4, e4, f4, g4,
	0x0, a5, b5, c5, d5, e5, f5, g5,
	0x0, a6, b6, c6, d6, e6, f6, g6,
	0x0, a7, b7, c7, d7, e7, f7, g7,
	0x0, a8, b8, c8, d8, e8, f8, g8,
	0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
};

static constexpr u64 kingWestTable[64] = {
	0x0, a1, b1, c1, d1, e1, f1, g1,
	0x0, a2, b2, c2, d2, e2, f2, g2,
	0x0, a3, b3, c3, d3, e3, f3, g3,
	0x0, a4, b4, c4, d4, e4, f4, g4,
	0x0, a5, b5, c5, d5, e5, f5, g5,
	0x0, a6, b6, c6, d6, e6, f6, g6,
	0x0, a7, b7, c7, d7, e7, f7, g7,
	0x0, a8, b8, c8, d8, e8, f8, g8,
};

static constexpr u64 kingSouthwestTable[64] = {
	0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
	0x0, a1, b1, c1, d1, e1, f1, g1,
	0x0, a2, b2, c2, d2, e2, f2, g2,
	0x0, a3, b3, c3, d3, e3, f3, g3,
	0x0, a4, b4, c4, d4, e4, f4, g4,
	0x0, a5, b5, c5, d5, e5, f5, g5,
	0x0, a6, b6, c6, d6, e6, f6, g6,
	0x0, a7, b7, c7, d7, e7, f7, g7,
};


}


u64 PawnMoveTable::WhiteMoves(const u64 &square)
{
    return whitePawnMoveTable[index(square)];
}

u64 PawnMoveTable::WhiteCaptures(const u64 &square)
{
    return whitePawnAttackTable[index(square)];
}

u64 PawnMoveTable::BlackMoves(const u64 &square)
{
    return blackPawnMoveTable[index(square)];
}

u64 PawnMoveTable::BlackCaptures(const u64 &square)
{
    return blackPawnAttackTable[index(square)];
}

u64 RookMoveTable::allMoves(const u64 &square)
{
    int i = index(square);
    return rookNorthTable[i] | rookEastTable[i] | rookSouthTable[i] | rookWestTable[i];
}

u64 RookMoveTable::NMoves(const u64 &square)
{
    return rookNorthTable[index(square)];
}

u64 RookMoveTable::EMoves(const u64 &square)
{
    return rookEastTable[index(square)];
}

u64 RookMoveTable::SMoves(const u64 &square)
{
    return rookSouthTable[index(square)];
}

u64 RookMoveTable::WMoves(const u64 &square)
{
    return rookWestTable[index(square)];
}

u64 KnightMoveTable::allMoves(const u64 &square)
{
    int i = index(square);
    return knightNortheastTable[i] | knightSoutheastTable[i] | knightSouthwestTable[i] | knightNorthwestTable[i];
}

u64 KnightMoveTable::NEMoves(const u64 &square)
{
    return knightNortheastTable[index(square)];
}

u64 KnightMoveTable::SEMoves(const u64 &square)
{
    return knightSoutheastTable[index(square)];
}

u64 KnightMoveTable::SWMoves(const u64 &square)
{
    return knightSouthwestTable[index(square)];
}

u64 KnightMoveTable::NWMoves(const u64 &square)
{
    return knightNorthwestTable[index(square)];
}

u64 BishopMoveTable::allMoves(const u64 &square)
{
    int i = index(square);
    return bishopNortheastTable[i] | bishopSoutheastTable[i] | bishopSouthwestTable[i] | bishopNorthwestTable[i];
}

u64 BishopMoveTable::NEMoves(const u64 &square)
{
    return bishopNortheastTable[index(square)];
}

u64 BishopMoveTable::SEMoves(const u64 &square)
{
    return bishopSoutheastTable[index(square)];
}

u64 BishopMoveTable::SWMoves(const u64 &square)
{
    return bishopSouthwestTable[index(square)];
}

u64 BishopMoveTable::NWMoves(const u64 &square)
{
    return bishopNorthwestTable[index(square)];
}

u64 QueenMoveTable::allMoves(const u64 &square)
{
    int i = index(square);
    return bishopNortheastTable[i] | bishopSoutheastTable[i] | bishopSouthwestTable[i] | bishopNorthwestTable[i] |
        rookNorthTable[i] | rookEastTable[i] | rookSouthTable[i] | rookWestTable[i];
}

u64 QueenMoveTable::NMoves(const u64 &square)
{
    return rookNorthTable[index(square)];
}

u64 QueenMoveTable::NEMoves(const u64 &square)
{
    return bishopNortheastTable[index(square)];
}

u64 QueenMoveTable::EMoves(const u64 &square)
{
    return rookEastTable[index(square)];
}

u64 QueenMoveTable::SEMoves(const u64 &square)
{
    return bishopSoutheastTable[index(square)];
}

u64 QueenMoveTable::SMoves(const u64 &square)
{
    return rookSouthTable[index(square)];
}

u64 QueenMoveTable::SWMoves(const u64 &square)
{
    return bishopSouthwestTable[index(square)];
}

u64 QueenMoveTable::WMoves(const u64 &square)
{
    return rookWestTable[index(square)];
}

u64 QueenMoveTable::NWMoves(const u64 &square)
{
    return bishopNorthwestTable[index(square)];
}

u64 KingMoveTable::allMoves(const u64 &square)
{
    int i = index(square);
    return kingNorthTable[i] | kingNortheastTable[i] | kingEastTable[i] | kingSoutheastTable[i] |
           kingSouthTable[i] | kingSouthwestTable[i] | kingWestTable[i] | kingNorthwestTable[i];
}

u64 KingMoveTable::NMoves(const u64 &square)
{
    return kingNorthTable[index(square)];
}

u64 KingMoveTable::NEMoves(const u64 &square)
{
    return kingNortheastTable[index(square)];
}

u64 KingMoveTable::EMoves(const u64 &square)
{
    return kingEastTable[index(square)];
}

u64 KingMoveTable::SEMoves(const u64 &square)
{
    return kingSoutheastTable[index(square)];
}

u64 KingMoveTable::SMoves(const u64 &square)
{
    return kingSouthTable[index(square)];
}

u64 KingMoveTable::SWMoves(const u64 &square)
{
    return kingSouthwestTable[index(square)];
}

u64 KingMoveTable::WMoves(const u64 &square)
{
    return kingWestTable[index(square)];
}

u64 KingMoveTable::NWMoves(const u64 &square)
{
    return kingNorthwestTable[index(square)];
}
