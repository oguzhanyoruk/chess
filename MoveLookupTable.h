#ifndef MOVELOOKUPTABLE_H
#define MOVELOOKUPTABLE_H

#include "Masks.h"
namespace mlt {
int index(u64 const& square);
}

namespace PawnMoveTable {
    u64   WhiteMoves(u64 const& square);
    u64   WhiteCaptures(u64 const& square);
    u64   BlackMoves(u64 const& square);
    u64   BlackCaptures(u64 const& square);
}

namespace RookMoveTable
{
    u64 allMoves(u64 const& square);
    u64   NMoves(u64 const& square);
    u64   EMoves(u64 const& square);
    u64   SMoves(u64 const& square);
    u64   WMoves(u64 const& square);
}

namespace KnightMoveTable {
    u64 allMoves(u64 const& square);
    u64  NEMoves(u64 const& square);
    u64  SEMoves(u64 const& square);
    u64  SWMoves(u64 const& square);
    u64  NWMoves(u64 const& square);
}

namespace BishopMoveTable {
    u64 allMoves(u64 const& square);
    u64  NEMoves(u64 const& square);
    u64  SEMoves(u64 const& square);
    u64  SWMoves(u64 const& square);
    u64  NWMoves(u64 const& square);
}

namespace QueenMoveTable {
    u64 allMoves(u64 const& square);
    u64   NMoves(u64 const& square);
    u64  NEMoves(u64 const& square);
    u64   EMoves(u64 const& square);
    u64  SEMoves(u64 const& square);
    u64   SMoves(u64 const& square);
    u64  SWMoves(u64 const& square);
    u64   WMoves(u64 const& square);
    u64  NWMoves(u64 const& square);
}

namespace KingMoveTable {
u64 allMoves(u64 const& square);
u64   NMoves(u64 const& square);
u64  NEMoves(u64 const& square);
u64   EMoves(u64 const& square);
u64  SEMoves(u64 const& square);
u64   SMoves(u64 const& square);
u64  SWMoves(u64 const& square);
u64   WMoves(u64 const& square);
u64  NWMoves(u64 const& square);
}

#endif //MOVELOOKUPTABLE_H
