#include "Player.h"
#include "Board.h"
#include "JThread.h"

#include <QElapsedTimer>
#include <limits>

unsigned int bestMoveIndex(std::vector<Evaluation> const& evaluations, PieceColor color) {
    auto accuracy = evaluations;
    auto point = evaluations;
    std::sort(accuracy.begin(), accuracy.end(), &Evaluation::moreAccurate);
    if (color == White) {
        std::sort(point.begin(), point.end(), &Evaluation::morePoint);
    } else {
        std::sort(point.begin(), point.end(), &Evaluation::lessPoint);
    }
    auto mostAccurate = accuracy.front();
    auto mostPoints = point.front();

    auto size = point.size();
    auto marginPoints =  mostPoints.evaluation * 0.05;
    auto marginAccuracy =  mostAccurate.accuracy * 0.10;
    for (unsigned int i = 0; i < size; ++i) {
        auto candidate = point[i];
        if (mostAccurate.accuracy - candidate.accuracy > marginAccuracy) {
            continue;
        }
        if (std::abs(mostPoints.evaluation - candidate.evaluation) < marginPoints) {
            return candidate.index;
        }
    }
    return mostPoints.index;
}

struct CreateSearcher {
    CreateSearcher(unsigned int i, MCTSPlayer* player, Board const& board)
        : i(i),
          player(player),
          board(board)
    {}
    void operator()() {
        int trialCount = 0;
        while (trialCount < maxTrialCount) {
            randomGame(board.clone());
            ++trialCount;
        }
        player->evaluations[i].accuracy = static_cast<double>(totalConclusiveTrial) / maxTrialCount;
        player->evaluations[i].index = i;
    }
private:
    void randomGame(Board cloneBoard) {
        int depth = 0;
        auto moves = cloneBoard.legalMoves();
        if (!moves.empty()) {
            do {
                auto move = moves[player->distribution[moves.size()](player->generator)];
                cloneBoard.moveUnsafe(move.from, move.to);
                moves = cloneBoard.legalMoves();
                ++depth;
            }
            while (!moves.empty() && depth < finalDepth);
        }
        Board::Status status = cloneBoard.getStatus();
        if (Board::WHITE_TURN == status || Board::BLACK_TURN == status) {
            if (player->getColor()) {
                player->evaluations[i].evaluation -= 0.5;
            } else {
                player->evaluations[i].evaluation += 0.5;
            }
            return;
        }
        if (Board::WHITE_WON == status) {
            player->evaluations[i].evaluation += (10.0 * (static_cast<double>(finalDepth) / depth));
        } else if (Board::BLACK_WON == status) {
            player->evaluations[i].evaluation -= (10.0 * (static_cast<double>(finalDepth) / depth));
        }
        ++totalConclusiveTrial;
    }
    unsigned int i;
    unsigned int totalConclusiveTrial = 0;
    int const maxTrialCount = 1000;
    const int finalDepth = 100;
    MCTSPlayer* player;
    Board board;
};


Player::Player(PieceColor color)
    : color(color)
{
}

PieceColor Player::getColor()
{
    return color;
}

Move RandomPlayer::pick(const Board &board)
{
    auto moves = board.legalMoves();
    auto random = rand() % static_cast<int>(moves.size());
    return moves[random];
}

MCTSPlayer::MCTSPlayer(PieceColor color)
    : Player(color)
{
    std::random_device rd;
    generator = std::mt19937(rd());
    for (unsigned int i = 1; i < 110; ++i) {
        distribution[i] = std::uniform_int_distribution<unsigned int>(0, i - 1);
    }
}

MCTSPlayer::~MCTSPlayer()
{
}

Move MCTSPlayer::pick(const Board &board)
{
    auto moves = board.legalMoves();
    auto size = moves.size();
    evaluations.clear();
    evaluations.resize(moves.size());
    QElapsedTimer timer;
    timer.start();
    {
        std::vector<JThread> threads;
        for (unsigned int i = 0; i < size; ++i) {
            Board cloneBoard = board.clone();
            auto const& move = moves[i];
            cloneBoard.moveUnsafe(move.from, move.to);
            //CreateSearcher(i, evaluations, this, cloneBoard)();
            threads.emplace_back(CreateSearcher(i, this, cloneBoard));
        }
    }
    Move ret = moves[bestMoveIndex(evaluations, color)];
    elapsed = timer.elapsed() / 1000.0;
    return ret;
}

std::vector<Evaluation> const& MCTSPlayer::getEvaluations()
{
    return evaluations;
}

double MCTSPlayer::elapsedTime()
{
    return elapsed;
}

bool Evaluation::moreAccurate(const Evaluation &lhs, const Evaluation &rhs)
{
    return lhs.accuracy > rhs.accuracy;
}

bool Evaluation::morePoint(const Evaluation &lhs, const Evaluation &rhs)
{
    return lhs.evaluation > rhs.evaluation;
}

bool Evaluation::lessPoint(const Evaluation &lhs, const Evaluation &rhs)
{
    return lhs.evaluation < rhs.evaluation;
}
