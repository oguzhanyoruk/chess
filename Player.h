#ifndef PLAYER_H
#define PLAYER_H

#include "Move.h"
#include <random>
#include <unordered_map>

class Board;


struct Evaluation {
    double accuracy;
    double evaluation;
    unsigned int index;
    static bool moreAccurate(Evaluation const& lhs, Evaluation const& rhs);
    static bool morePoint(Evaluation const& lhs, Evaluation const& rhs);
    static bool lessPoint(Evaluation const& lhs, Evaluation const& rhs);
};

class Player
{
public:
    Player(PieceColor color = Black);
    virtual Move pick(Board const& board) = 0;
    PieceColor getColor();
protected:
    PieceColor color;
};

class RandomPlayer : public Player
{
public:
    Move pick(Board const& board) override;
};

class MCTSPlayer : public Player {
public:
    MCTSPlayer(PieceColor color = Black);
    ~MCTSPlayer();
    Move pick(Board const& board) override;
    std::vector<Evaluation> const& getEvaluations();
    double elapsedTime();

private:
    friend struct CreateSearcher;
    std::mt19937 generator;
    std::vector<Evaluation> evaluations;
    double elapsed;
    std::unordered_map<unsigned int, std::uniform_int_distribution<unsigned int>> distribution;
};

#endif // PLAYER_H
