#include "Settings.h"


Settings::Settings() : QObject(nullptr)
{
//    Dark 184,139,74
//    Light 227,193,111
    _darkColor = QColor(184, 139, 74);
    _lightColor = QColor(227, 193, 111);
}

QColor Settings::darkColor()
{
    return _darkColor;
}

void Settings::setDarkColor(const QColor &value)
{
    if (value != _darkColor) {
        _darkColor = value;
        emit darkColorChanged(value);
    }
}

QColor Settings::lightColor()
{
    return _lightColor;
}

void Settings::setLightColor(const QColor &value)
{
    if (value != _lightColor) {
        _lightColor = value;
        emit lightColorChanged(value);
    }
}

std::shared_ptr<Settings> Settings::getSettings()
{
    static std::shared_ptr<Settings> _settings{new Settings};
    return _settings;
}
