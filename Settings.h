#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QColor>
#include <memory>

#define SettingsPtr Settings::getSettings()

class Settings : public QObject
{
    Q_OBJECT
public:
    Q_PROPERTY(QColor darkColor READ darkColor WRITE setDarkColor NOTIFY darkColorChanged)
    Q_PROPERTY(QColor lightColor READ lightColor WRITE setLightColor NOTIFY lightColorChanged)

    QColor darkColor();
    void setDarkColor(QColor const &value);
    QColor lightColor();
    void setLightColor(QColor const &value);
    static std::shared_ptr<Settings> getSettings();
    Settings(Settings const&) = delete;
    Settings(Settings&&) = delete;
    Settings& operator=(Settings const&) = delete;
    Settings& operator=(Settings&&) = delete;
signals:
    void darkColorChanged(QColor value);
    void lightColorChanged(QColor value);

private:
    Settings();
    QColor _darkColor;
    QColor _lightColor;
};

#endif // SETTINGS_H
