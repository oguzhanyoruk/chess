#include "Square.h"

#include <QPen>
#include <QBrush>
#include <QPainter>

#include "Settings.h"

Square::Square(QQuickItem* parent)
    : QQuickPaintedItem(parent)
    , _length(0)
    , _fillBrush(std::make_unique<QBrush>(Qt::transparent))
{
    setFlag(ItemHasContents);
    auto onValueChanged = [this](auto){
        this->update();
    };
    setAcceptedMouseButtons(Qt::AllButtons);
    _settings = SettingsPtr;
    connect(this, &Square::lengthChanged, this, onValueChanged);
    connect(_settings.get(), &Settings::darkColorChanged, this, onValueChanged);
    connect(_settings.get(), &Settings::lightColorChanged, this, onValueChanged);
}

Square::~Square()
{

}

int Square::length()
{
    return _length;
}

void Square::setLength(int value) {
    if (_length != value) {
        _length = value;
        setWidth(_length);
        setHeight(_length);
        emit lengthChanged(value);
    }
}

Square::Color Square::color()
{
    return _color;
}

void Square::setColor(int value)
{
    if (value != _color) {
        _color = static_cast<Square::Color>(value);
        emit colorChanged(_color);
    }
}

void Square::paint(QPainter *painter)
{
    this->setBrush();
    painter->setRenderHint(QPainter::Antialiasing);
    painter->setBrush(*_fillBrush);
    painter->drawRect(0, 0, _length, _length);
}

void Square::setBrush()
{
    _fillBrush->setColor(_color == Color::Dark ? SettingsPtr->darkColor() : SettingsPtr->lightColor());
}

