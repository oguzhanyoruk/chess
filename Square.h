#ifndef SQUARE_H
#define SQUARE_H

#include <QQuickPaintedItem>
#include <QColor>
#include <QPointF>
#include <memory>

class Settings;

class Square : public QQuickPaintedItem {
    Q_OBJECT
public:
    enum Color {Dark, Light};
    Q_ENUM(Color)
    Q_PROPERTY(int length READ length WRITE setLength NOTIFY lengthChanged)
    Q_PROPERTY(Color color READ color WRITE setColor NOTIFY colorChanged)
    Square(QQuickItem *parent = nullptr);
    ~Square();
    void paint(QPainter *painter) override;
    int length();
    void setLength(int value);
    Color color();
    void setColor(int value);

signals:
    void lengthChanged(double value);
    void colorChanged(Color value);

protected:
    void setBrush();
    double _length;
    Color _color;
    QColor _fillColor;
    std::unique_ptr<QBrush> _fillBrush;
    std::shared_ptr<Settings> _settings;
};


#endif // SQUARE_H
