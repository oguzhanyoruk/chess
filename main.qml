import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.1
import Board.Blocks 1.0

ApplicationWindow {
    width: 480
    height: 480
    visible: true
    title: qsTr("Chess")
    minimumHeight: 480
    minimumWidth: 640
    Board {
        id: board
        anchors.left: parent.left
        anchors.top: parent.top
        side: parent.height < parent.width - 160 ? parent.height : parent.width - 160;
    }
    InfoTable {
        id: infoTable
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        width: parent.width - board.side
    }
}
